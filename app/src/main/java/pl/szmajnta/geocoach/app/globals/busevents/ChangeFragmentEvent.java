package pl.szmajnta.geocoach.app.globals.busevents;

import android.support.v4.app.Fragment;

/**
 * Created by Andrzej on 2015-04-18.
 */
public class ChangeFragmentEvent {

    private Fragment requestedFragment;
    private boolean animationEnable;

    public ChangeFragmentEvent(Fragment requestedFragment, boolean animationEnable) {
        this.requestedFragment = requestedFragment;
        this.animationEnable = animationEnable;
    }

    public ChangeFragmentEvent(Fragment requestedFragment) {
        this.requestedFragment = requestedFragment;
        this.animationEnable = true;
    }

    public Fragment getRequestedFragment() {
        return requestedFragment;
    }

    public void setRequestedFragment(Fragment requestedFragment) {
        this.requestedFragment = requestedFragment;
    }

    public boolean isAnimationEnable() {
        return animationEnable;
    }

    public void setAnimationEnable(boolean animationEnable) {
        this.animationEnable = animationEnable;
    }
}
