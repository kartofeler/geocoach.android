package pl.szmajnta.geocoach.app.Model.Entities;

import android.location.Location;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-05-24.
 */
public class TrainingNode {

    public final static String COLUMN_ID = "id";
    public final static String COLUMN_LAT = "latitude";
    public final static String COLUMN_LONG = "longitude";
    public final static String COLUMN_TEXT = "locationText";
    public final static String COLUMN_DESC = "description";



    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private int id;
    @DatabaseField(columnName = COLUMN_LONG)
    private float longitude;
    @DatabaseField(columnName = COLUMN_LAT)
    private float latitude;
    @DatabaseField(columnName = COLUMN_TEXT)
    private String locationText = "";
    @DatabaseField(columnName = COLUMN_DESC)
    private String description = "";


    public TrainingNode() {
    }

    public TrainingNode(float latitude, float longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getLocationText() {
        return locationText;
    }

    public void setLocationText(String locationText) {
        this.locationText = locationText;
    }

    public TrainingNode setDescription(String description) {
        this.description = description;
        return this;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public Location getLocation(){
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        return location;
    }
}
