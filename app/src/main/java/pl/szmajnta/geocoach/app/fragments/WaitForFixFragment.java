package pl.szmajnta.geocoach.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dd.processbutton.iml.ActionProcessButton;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.api.rest.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import org.springframework.web.client.HttpClientErrorException;

import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.beans.GeocoachUser;
import pl.szmajnta.geocoach.app.globals.EventBus;
import pl.szmajnta.geocoach.app.globals.UserPreferences_;
import pl.szmajnta.geocoach.app.globals.busevents.ChangeFragmentEvent;
import pl.szmajnta.geocoach.app.restapi.SecurityApi;
import pl.szmajnta.geocoach.app.restapi.model.request.LoginData;
import pl.szmajnta.geocoach.app.restapi.model.response.LoginResponse;

@EFragment(R.layout.fragment_login)
public class WaitForFixFragment extends Fragment {

    @Bean EventBus bus;

    public static WaitForFixFragment newInstance() {
        WaitForFixFragment fragment = new WaitForFixFragment();
        return fragment;
    }

    public WaitForFixFragment() {
    }

    @Subscribe
    public void onAccuracyChanged(float accuracy){

    }

}
