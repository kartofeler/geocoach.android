package pl.szmajnta.geocoach.app.Model.Filters;

import pl.szmajnta.geocoach.app.Model.Entities.Interval;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-08-09.
 */
public interface IntervalPartFilter {
    public List<Interval> sort(List<Interval> list);
}
