package pl.szmajnta.geocoach.app.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.Statistic.StatisticActivity;
import pl.szmajnta.geocoach.app.Tracking.Activities.TrainingCreation.IntervalTemplatesShowActivity;
import pl.szmajnta.geocoach.app.Tracking.Activities.TrainingCreation.RouteSettingsActivity;
import pl.szmajnta.geocoach.app.VibrateUtils;

public class SplashActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_splash);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.splash_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.messages:
                buttonMessageClick(null);
                return true;
            case R.id.help:
                buttonHelpClick(null);
                return true;
            case R.id.settings:
                buttonOptionsClick(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void buttonNewRouteClick(View view){
        Intent intent = new Intent(this, RouteSettingsActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }

    public void buttonStatisticClick(View view){
        Intent intent = new Intent(this, StatisticActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }

    public void buttonOptionsClick(View view){
        Intent intent = new Intent(this, ConfigureActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }

    public void buttonEditorClick(View view){
        Intent intent = new Intent(this, NodeTemplatesShowActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }

    public void buttonIntervClick(View view){
        Intent intent = new Intent(this, IntervalTemplatesShowActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }

    public void buttonMessageClick(View view){
        Intent intent = new Intent(this, MessageActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }

    public void buttonHelpClick(View view){
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
        VibrateUtils.VibrateShort(this);
    }
}
