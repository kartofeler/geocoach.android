package pl.szmajnta.geocoach.app.CommonHelpers;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by The Ja on 2014-06-08.
 */
public class PopUpHelper {
    public static void displayInfoPopUp(Context context, String message){
        new AlertDialog.Builder(context)
                .setMessage(message)
                .show();
    }

    public static void displayInfoPopUp(Context context, String message, String title){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .show();
    }
}
