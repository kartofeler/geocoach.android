package pl.szmajnta.geocoach.app.Model;

import android.os.AsyncTask;
import pl.szmajnta.geocoach.app.Interfaces.TrainingProgressCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-06-08.
 */
public class SpecializedProgressArchiver extends BaseProgressArchiver {

    private List<Double> kmsTimes;
    private double lastKm = 0;

    public SpecializedProgressArchiver(int gpsInterval) {
        super(gpsInterval);
        kmsTimes = new ArrayList<Double>();
    }

    @Override
    public void locationChangedEvent(int time, MeasureSample measureSample) {
        if(measureSample != null){
            distanceHistory.add(measureSample.totalDistance);
            paceHistory.add( 60 / (measureSample.momentVelocity * 3.6) );
            altitudeHistory.add(measureSample.altitude);

            if(measureSample.totalDistance - lastKm > 1000){
                kmsTimes.add((double)time);
                lastKm = measureSample.totalDistance;
            }
        }
    }

    @Override
    public List<Double> getKmsTimes() {
        return kmsTimes;
    }

    @Override
    public void getBestTimeForOneKm(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchForShortestTime(1);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestTimeForTwoKm(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchForShortestTime(2);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestTimeForFiveKm(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchForShortestTime(5);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestTimeForTenKm(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchForShortestTime(10);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestTimeForTwentyKm(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchForShortestTime(20);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestTimeForMaraton(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchForShortestTime(42.197);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestDistanceForFiveMin(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchDistanceForTime(1);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestDistanceForCouper(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchDistanceForTime(12*60);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestDistanceForHalfHour(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchDistanceForTime(30*60);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    @Override
    public void getBestDistanceForHour(final TrainingProgressCallback callback) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                double value = searchDistanceForTime(60*60);
                callback.resultCalculationDone(value);
                return null;
            }
        }.execute();
    }

    private int searchForShortestTime(double distanceInKm){
        double minTime = distanceHistory.size() * gpsInterval;
        for(int i = 0; i < distanceHistory.size(); i++){

            double value = distanceHistory.get(i);

            for(int j = i+1; j < distanceHistory.size(); j++){
                double newValue = distanceHistory.get(j);
                if(newValue - value > distanceInKm * 1000){
                    if(j - i < minTime){
                        return (j-i)*gpsInterval;
                    }
                }
            }
        }
        return 0;
    }

    private double searchDistanceForTime(int timeInSeconds){
        int samplesCount = timeInSeconds / gpsInterval;

        double distanceMax = 0;
        for(int i = 0; i < distanceHistory.size(); i++){
            double distanceI = distanceHistory.get(i);
            double distanceJ = distanceHistory.get(i + samplesCount);
            if(distanceJ - distanceI > distanceMax){
                distanceMax = distanceJ - distanceI;
            }
        }
        return distanceMax;
    }
}
