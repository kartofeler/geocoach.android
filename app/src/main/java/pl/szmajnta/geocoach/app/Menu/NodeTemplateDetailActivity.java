package pl.szmajnta.geocoach.app.Menu;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.NodesTemplate;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.VibrateUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-06-01.
 */
public class NodeTemplateDetailActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> {
    private GoogleMap map;

    private TrackerDatabaseHelper helper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_add_node_template);

        helper = getHelper();

        int templateId = getIntent().getIntExtra("templateId", -1);
        if(templateId != -1) {
            NodesTemplate template = helper.getNodesTemplateById(templateId);

            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            PolylineOptions line = new PolylineOptions();
            line.color(getResources().getColor(R.color.pawel_green));
            List<TrainingNode> nodes = null;
            try {
                nodes = helper.getNodesForTemplate(template);
                LatLng last = new LatLng(nodes.get(0).getLatitude(), nodes.get(0).getLongitude());
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(last, 16));
                for(TrainingNode node : nodes){
                    map.addMarker(new MarkerOptions()
                            .title(node.getDescription())
                            .position(new LatLng(node.getLatitude(), node.getLongitude()))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));
                    line.add(new LatLng(node.getLatitude(), node.getLongitude()));
                }
                map.addPolyline(line);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            updateValuesOnBottom(template.getDescription(), nodes.size());

            EditText nameEdit = (EditText)findViewById(R.id.etTemplateName);
            nameEdit.setText(template.getName());
            nameEdit.setEnabled(false);
        }


        ImageButton saveButton = (ImageButton)findViewById(R.id.add_button);
        saveButton.setVisibility(View.INVISIBLE);

        ImageButton clearAllButton = (ImageButton)findViewById(R.id.remove_button);
        clearAllButton.setVisibility(View.INVISIBLE);

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(NodeTemplateDetailActivity.this);
                onBackPressed();
            }
        });
    }

    private void updateValuesOnBottom(final String distance,final int count) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tvDistance = (TextView)findViewById(R.id.tvDistance);
                DecimalFormat formatter = new DecimalFormat("0.00km");

                TextView tvPoints = (TextView)findViewById(R.id.tvCount);

                tvDistance.setText("Dystans:" + distance);
                tvPoints.setText("Punktów:" + String.valueOf(count));
            }
        });
    }
}