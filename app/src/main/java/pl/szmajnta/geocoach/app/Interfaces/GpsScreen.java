package pl.szmajnta.geocoach.app.Interfaces;

/**
 * Created by Andrzej Laptop
 */
public interface GpsScreen {
    public void gpsStatusChange(float distance);
}
