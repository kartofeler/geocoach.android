package pl.szmajnta.geocoach.app.Tracking.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import pl.szmajnta.geocoach.app.Interfaces.MapScreen;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;
import pl.szmajnta.geocoach.app.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import java.util.List;

/**
 * Created by Andrzej Laptop
 *
 */
public class MapFragment extends BaseTrackerFragment implements MapScreen {

    private static final String LAST_ZOOM = "last_zoom";
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_map, container, false);

        if(mMapFragment == null) {
            mMapFragment = SupportMapFragment.newInstance();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.map_container, mMapFragment).commit();
        }

        return view;
    }

    @Override
    public void onStart(){
        super.onStart();

        if(mMap == null){
            mMap = mMapFragment.getMap();
            mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    saveZoomToPreferences(cameraPosition.zoom);
                }
            });
        }

        drawRoute();
    }

    private void saveZoomToPreferences(float zoom){
        SharedPreferences.Editor editor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
        editor.putFloat(LAST_ZOOM, zoom);
        editor.commit();
    }

    private float getZoomFromPreferences(){
        SharedPreferences pref = getActivity().getPreferences(Context.MODE_PRIVATE);
        return pref.getFloat(LAST_ZOOM, 15);
    }

    @Override
    public void onResume(){
        super.onResume();

        if(mMap == null) {
            mMap = mMapFragment.getMap();
        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        List<TrainingNode> nodes = getTrackerActivity().getCurrentTraining().getAllNodes();
        int currNode = getTrackerActivity().getCurrentTraining().getCurrentNodeIndex();

        if(nodes.size() > 0) {
            int i = 0;
            for (TrainingNode node : nodes) {
                if(i < currNode) {
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(node.getLatitude(), node.getLongitude()))
                            .title(node.getDescription())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_light)));
                }if(i == currNode){
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(node.getLatitude(), node.getLongitude()))
                            .title(node.getDescription())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));
                }
                if(i > currNode){
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(node.getLatitude(), node.getLongitude()))
                            .title(node.getDescription())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_dark)));
                }
                i++;
            }

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(nodes.get(currNode).getLatitude(), nodes.get(currNode).getLongitude()), getZoomFromPreferences()));
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void drawRoute() {

        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                List<Location> points = getTrackerActivity().getCurrentTraining().getAllLocation();
                if(points.size() > 0) {
                    final PolylineOptions lineOptions = new PolylineOptions();
                    lineOptions.color(R.color.pawel_green);
                    for (Location loc : points) {

                        lineOptions.add(new LatLng(loc.getLatitude(), loc.getLongitude()));
                    }
                    final Location loc = points.get(points.size() - 1);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), getZoomFromPreferences()));
                            mMap.addPolyline(lineOptions);
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.567574, 20.040225), getZoomFromPreferences()));
                        }
                    });
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void updateRoute() {
        drawRoute();
    }
}