package pl.szmajnta.geocoach.app.Tracking.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public abstract class TrackerPagerAdapter extends FragmentPagerAdapter {

    abstract public String[] getHeader();

    public TrackerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        return null;
    }

    @Override
    abstract public int getCount();
}

