package pl.szmajnta.geocoach.app.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.Tracking.Activities.TrainingCreation.RouteSettingsActivity;

import java.util.ArrayList;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class HelpActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_help);
    }

    public void buttonStartClick(View view){
        Intent intent = new Intent(this, RouteSettingsActivity.class);
        startActivity(intent);
    }

    public void buttonBackClick(View view){
        super.onBackPressed();
    }
}