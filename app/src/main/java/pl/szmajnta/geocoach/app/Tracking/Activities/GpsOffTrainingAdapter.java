package pl.szmajnta.geocoach.app.Tracking.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import pl.szmajnta.geocoach.app.Tracking.Fragments.*;

import java.util.ArrayList;

public class GpsOffTrainingAdapter extends TrackerPagerAdapter{

    ArrayList<Fragment> fragments = new ArrayList<Fragment>();

    @Override
    public  String[] getHeader() {
        return new String[]{"Kompas"};
    }

    public GpsOffTrainingAdapter(FragmentManager fm){
        super(fm);
        fragments.add(new CompassFragment());
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0: return "Kompas";
        }

        return null;
    }

    @Override
    public Fragment getItem(int index) {
        if(index >= fragments.size()) return null;
        Fragment frag = fragments.get(index);
        if (frag == null) {
            switch (index) {
                case 0:
                    frag = new CompassFragment();
                    break;
            }
        }
        return frag;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
