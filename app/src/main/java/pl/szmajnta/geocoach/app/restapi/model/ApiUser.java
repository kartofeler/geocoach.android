package pl.szmajnta.geocoach.app.restapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrzej on 2015-04-13.
 */
public class ApiUser {
    @JsonProperty()
    private int id;
    @JsonProperty()
    private String email;
    @JsonProperty()
    private String username;
    @JsonProperty()
    private String gravatarUrl;

    public ApiUser(int id, String email, String username, String gravatarUrl) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.gravatarUrl = gravatarUrl;
    }

    public ApiUser() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGravatarUrl() {
        return gravatarUrl;
    }

    public void setGravatarUrl(String gravatarUrl) {
        this.gravatarUrl = gravatarUrl;
    }
}
