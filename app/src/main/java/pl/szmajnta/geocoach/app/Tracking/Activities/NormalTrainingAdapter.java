package pl.szmajnta.geocoach.app.Tracking.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import pl.szmajnta.geocoach.app.Tracking.Fragments.ChartsFragment;
import pl.szmajnta.geocoach.app.Tracking.Fragments.CompassFragment;
import pl.szmajnta.geocoach.app.Tracking.Fragments.MapFragment;
import pl.szmajnta.geocoach.app.Tracking.Fragments.MeasureFragment;

import java.util.ArrayList;

public class NormalTrainingAdapter extends TrackerPagerAdapter {

    ArrayList<Fragment> fragments = new ArrayList<Fragment>();

    @Override
    public String[] getHeader() {
        return new String[]{"Kompas",  "Pomiary", "Mapa","Wykresy"};
    }

    public NormalTrainingAdapter(FragmentManager fm) {
        super(fm);
        fragments.add(new CompassFragment());
        fragments.add(new MeasureFragment());
        fragments.add(new MapFragment());
        fragments.add(new ChartsFragment());
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0: return "Kompas";
            case 1: return "Pomiary";
            case 2: return "Mapa";
            case 3: return "Wykresy";
        }

        return null;
    }

    @Override
    public Fragment getItem(int index) {
        if(index >= fragments.size() || index < 0) return null;
        Fragment frag = fragments.get(index);
        if (frag == null) {
            switch (index) {
                case 0:
                    frag = new CompassFragment();
                    break;
                case 1:
                    frag = new MeasureFragment();
                    break;
                case 2:
                    frag = new MapFragment();
                    break;
                case 3:
                    frag = new ChartsFragment();
                    break;
            }
        }
        return frag;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
