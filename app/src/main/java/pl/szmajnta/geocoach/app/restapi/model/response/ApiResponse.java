package pl.szmajnta.geocoach.app.restapi.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrzej on 2015-04-13.
 */
public class ApiResponse {
    @JsonProperty("code")
    protected int code;
    @JsonProperty("description")
    protected String description;

    public ApiResponse() {
    }

    public ApiResponse(int code, String description) {
        this.code = code;
        this.description = description;
    }
    @JsonProperty("code")
    public int getCode() {
        return code;
    }
    @JsonProperty("code")
    public void setCode(int code) {
        this.code = code;
    }
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }
}
