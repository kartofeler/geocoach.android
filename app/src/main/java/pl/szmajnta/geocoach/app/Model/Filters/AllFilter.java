package pl.szmajnta.geocoach.app.Model.Filters;

import pl.szmajnta.geocoach.app.Model.Entities.Interval;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Andrzej Laptop
 */
public class AllFilter implements IntervalPartFilter {

    @Override
    public List<Interval> sort(List<Interval> list) {
        Collections.sort(list, new Comparator<Interval>() {
            @Override
            public int compare(Interval interval, Interval interval2) {
                return interval.getValue() < interval2.getValue() ? -1
                        : interval.getValue() > interval2.getValue() ? 1
                        : 0;
            }
        });
        return list;
    }
}
