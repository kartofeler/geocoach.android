package pl.szmajnta.geocoach.app.Tracking.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import pl.szmajnta.geocoach.app.Interfaces.GpsScreen;
import pl.szmajnta.geocoach.app.Interfaces.MeasureScreen;
import pl.szmajnta.geocoach.app.Interfaces.OrientationScreen;
import pl.szmajnta.geocoach.app.Interfaces.TrainingModel;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.Training;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;
import pl.szmajnta.geocoach.app.Model.MeasureSample;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.Tracking.Activities.SummaryActivity;
import pl.szmajnta.geocoach.app.VibrateUtils;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-03.
 */
public class CompassFragment extends BaseTrackerFragment implements OrientationScreen, MeasureScreen, GpsScreen {

    private float currentDegree = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_compass, container, false);
        final TrainingModel model = getTrackerActivity().getCurrentTraining();

        final ImageView stopButton = (ImageView)view.findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(getTrackerActivity());
                AlertDialog.Builder confirmationWindow = new AlertDialog.Builder(getTrackerActivity());
                confirmationWindow.setMessage("Czy na pewno chcesz zakończyć i zapisać trening?")
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                ProgressDialog bar = new ProgressDialog(getTrackerActivity());
                                bar.setMessage("Trwa zapis do bazy");
                                bar.setIndeterminate(true);
                                bar.show();

                                model.stopTraining();
                                try {
                                    model.saveTrainingState(new TrackerDatabaseHelper(getTrackerActivity()));
                                } catch (Exception e) {
                                    Toast tost = Toast.makeText(getTrackerActivity(), "Zapis nie powiódł się!", Toast.LENGTH_SHORT);
                                    tost.show();
                                }
                                finally {
                                    bar.dismiss();

                                    List<Training> trainingList = new TrackerDatabaseHelper(getTrackerActivity()).getAllTraining();
                                    Training lastTraining = trainingList.get(trainingList.size()-1);

                                    Intent intent = new Intent(getTrackerActivity(), SummaryActivity.class);
                                    intent.putExtra("trainingId", lastTraining.getId());
                                    intent.putExtra("prevActivity", "tracking");
                                    startActivity(intent);
                                }
                            }
                        })
                        .setNegativeButton("Nie", null)
                        .show();
            }
        });
        return view;
    }

    public void onResume() {
        super.onResume();
        getTrackerActivity().getCurrentTraining().updateNodesToGui();
    }

    @Override
    public void newNorth(float zAxis) {
        rotateCompass(zAxis);
    }

    @Override
    public void newOrientation(float zAxis, float distance) {
        rotateCompass(zAxis);

        DecimalFormat decimalFormat = new DecimalFormat("0.0");

        TextView distanceView = (TextView)getView().findViewById(R.id.distanceTv);
        distanceView.setVisibility(View.VISIBLE);
        if(distance > 1000){
            distanceView.setText(decimalFormat.format(distance/1000) + "km");
        } else {
            distanceView.setText(decimalFormat.format(distance) + "m");
        }
    }

    @Override
    public void updateNodeInformation(int curr, int all, TrainingNode[] nodes) {
        if(getView() != null) {
            TextView countView = (TextView)getView().findViewById(R.id.nodeCountView);
            countView.setText(curr + "/" + all);
        }
    }

    private void rotateCompass(float value){
        try {
            ImageView imageView = (ImageView) getView().findViewById(R.id.imageViewCompass);
            RotateAnimation rotateAnimation = new RotateAnimation(currentDegree, -value, Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f);
            rotateAnimation.setDuration(100);

            currentDegree = -value;

            imageView.startAnimation(rotateAnimation);
        } catch(Exception ex){

        }
    }

    @Override
    public void newLocation(int time, MeasureSample sample) {
        TextView left_up = (TextView)getView().findViewById(R.id.valueTv1);
        TextView right_up = (TextView)getView().findViewById(R.id.valueTv2);
        TextView left_down = (TextView)getView().findViewById(R.id.valueTv3);
        TextView right_down = (TextView)getView().findViewById(R.id.valueTv4);

        float pace = (float)(60/(sample.meanVelocity * 3.6));
        DecimalFormat formatter = new DecimalFormat("#.00");
        left_up.setText(formatter.format(sample.totalDistance/1000));
        right_up.setText(formatter.format(pace));
        left_down.setText(formatter.format(sample.altitude));
        right_down.setText(formatter.format(sample.slope));

        newTime(time);
    }

    @Override
    public void newTime(int seconds) {
        TextView timeView = (TextView)getView().findViewById(R.id.timeTextView);
        int[] time = splitSeconds(seconds);
        String hourS, minuteS, secondS;
        DecimalFormat decimalFormat = new DecimalFormat("00");
        hourS = decimalFormat.format(time[0]);
        minuteS = decimalFormat.format(time[1]);
        secondS = decimalFormat.format(time[2]);
        timeView.setText(hourS + ":" + minuteS + ":" + secondS);
    }

    private int[] splitSeconds(int time) {
        int hours, minutes, seconds;

        hours = time / 3600;
        minutes = (time % 3600) / 60;
        seconds = (time % 3600) % 60;

        int[] arr = new int[3];

        arr[0] = hours;
        arr[1] = minutes;
        arr[2] = seconds;

        return arr;
    }

    @Override
    public void gpsStatusChange(float distance) {
        TextView accuracyText = (TextView) getView().findViewById(R.id.prox_text);
        ImageView signalStrengthView = (ImageView)getView().findViewById(R.id.gps_status);
        Drawable gpsSignalDraw = null;
        if(distance <= 100) {
            String decimalAccuracy = new DecimalFormat("#.#").format(distance);
            accuracyText.setText(String.valueOf(decimalAccuracy) + "m");
            if(distance < 15){
                gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_3);
            } else if(distance < 35){
                gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_2);
            } else{
                gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_1);
            }

        } else{
            accuracyText.setText("Oczekiwanie...");
            gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_weak);
        }
        if(gpsSignalDraw != null){
            signalStrengthView.setImageDrawable(gpsSignalDraw);
        }
    }
}