package pl.szmajnta.geocoach.app.Interfaces;

import pl.szmajnta.geocoach.app.Model.MeasureSample;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public interface TrainingModelListener {

    public void locationChangedEvent(int time, MeasureSample measureSample);
    public void directionChangedEvent(float zAxis, float distance);
    public void northChangedEvent (float zAxis);
    public void nodeChangedEvent(int currentNodeIndex, int nodesCount, TrainingNode[] nodes);
    public void timeChangedEvent(int seconds);
    public void gpsStatusChanged(float distance);
}
