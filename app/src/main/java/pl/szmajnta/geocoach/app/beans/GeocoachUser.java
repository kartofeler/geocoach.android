package pl.szmajnta.geocoach.app.beans;

import android.util.Log;
import com.squareup.otto.Produce;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.annotations.sharedpreferences.SharedPref;
import org.androidannotations.api.rest.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import pl.szmajnta.geocoach.app.globals.EventBus;
import pl.szmajnta.geocoach.app.globals.UserPreferences_;
import pl.szmajnta.geocoach.app.globals.busevents.UserInformationFetchedEvent;
import pl.szmajnta.geocoach.app.globals.busevents.UserLoginStatusEvent;
import pl.szmajnta.geocoach.app.restapi.GeocoachAPI;
import pl.szmajnta.geocoach.app.restapi.SecurityApi;
import pl.szmajnta.geocoach.app.restapi.model.AccessToken;
import pl.szmajnta.geocoach.app.restapi.model.ApiUser;
import pl.szmajnta.geocoach.app.restapi.model.request.LoginData;
import pl.szmajnta.geocoach.app.restapi.model.response.LoginResponse;
import pl.szmajnta.geocoach.app.restapi.model.response.UserInfoResponse;

/**
 * Created by Andrzej on 2015-04-18.
 */

@EBean(scope = EBean.Scope.Singleton)
public class GeocoachUser implements RestErrorHandler  {

    @RestService
    SecurityApi securityRest;
    @RestService GeocoachAPI geocoachRest;
    @Pref UserPreferences_ userCredentials;
    @Bean EventBus bus;

    private AccessToken token;
    private ApiUser userInformation;

    public GeocoachUser(){

    }

    @AfterInject
    void registerToBus(){
        bus.register(this);
    }

    public boolean isLogged(){
        return token != null;
    }

    public AccessToken getToken() {
        return token;
    }

    public void setToken(AccessToken token) {
        this.token = token;
        geocoachRest.setHeader("Authorization", token.getAccessToken());
        getCurrentUserForToken(token.getAccessToken());
        bus.post(new UserLoginStatusEvent(true));
    }

    public ApiUser getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(ApiUser userInformation) {
        this.userInformation = userInformation;
        bus.post(new UserInformationFetchedEvent(this.getUserInformation()));
    }

    private boolean credentialsAreComplete(){
        return !userCredentials.email().get().isEmpty() && !userCredentials.password().get().isEmpty();
    }

    @AfterInject
    void setRestErrorHandler(){
        securityRest.setRestErrorHandler(this);
        geocoachRest.setRestErrorHandler(this);
    }

    @Background
    @AfterInject
    void postLoginFromCredentials(){
        if(credentialsAreComplete()) {
            LoginResponse response = securityRest.loginAction(new LoginData(userCredentials.email().get(), userCredentials.password().get()));
            if (response != null) {
                this.setToken(response.getExtras());
                Log.d("Geocoach", userCredentials.email().get() + " logged");
            }
        }
    }

    @Background
    void getCurrentUserForToken(String token){
        UserInfoResponse response = geocoachRest.getUserInformationAction();
        if (response != null) {
            this.setUserInformation(response.getExtras());
            Log.d("Geocoach", userInformation.getUsername() + " information fetched");
        }
    }

    @Produce public UserLoginStatusEvent getLoginStatus(){
        return new UserLoginStatusEvent(this.isLogged());
    }

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {
        Log.d("Geocoach", e.getMessage());
    }
}
