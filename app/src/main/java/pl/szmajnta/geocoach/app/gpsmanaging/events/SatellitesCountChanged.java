package pl.szmajnta.geocoach.app.gpsmanaging.events;

import pl.szmajnta.geocoach.app.base.ValueChanged;

/**
 * Created by andrzej on 18.05.15.
 */
public class SatellitesCountChanged extends ValueChanged<Integer> {
    public SatellitesCountChanged(Integer count) {
        super(count);
    }
}
