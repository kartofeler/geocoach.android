package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-05-30.
 */
public class NodeForTemplateJoin {

    public final static String COLUMN_TEMPLATE_ID = "template_id";
    public final static String COLUMN_NODE_ID = "node_id";
    public final static String COLUMN_ID = "id";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private int id;
    @DatabaseField(foreign = true, columnName = COLUMN_TEMPLATE_ID, foreignAutoRefresh = true)
    private NodesTemplate template;
    @DatabaseField(foreign = true, columnName = COLUMN_NODE_ID, foreignAutoRefresh = true)
    private TrainingNode node;

    public NodeForTemplateJoin() {
    }

    public NodeForTemplateJoin(NodesTemplate template, TrainingNode node) {
        this.template = template;
        this.node = node;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NodesTemplate getTemplate() {
        return template;
    }

    public void setTemplate(NodesTemplate template) {
        this.template = template;
    }

    public TrainingNode getNode() {
        return node;
    }

    public void setNode(TrainingNode node) {
        this.node = node;
    }
}
