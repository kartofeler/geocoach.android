package pl.szmajnta.geocoach.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import de.hdodenhof.circleimageview.CircleImageView;
import org.androidannotations.annotations.*;
import org.androidannotations.annotations.sharedpreferences.Pref;

import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.Tracking.Activities.MainTrackerActivity;
import pl.szmajnta.geocoach.app.fragments.*;
import pl.szmajnta.geocoach.app.globals.EventBus;
import pl.szmajnta.geocoach.app.globals.GpsPreferences_;
import pl.szmajnta.geocoach.app.globals.busevents.ChangeFragmentEvent;
import pl.szmajnta.geocoach.app.globals.busevents.StartTrainingEvent;
import pl.szmajnta.geocoach.app.globals.busevents.UserInformationFetchedEvent;
import pl.szmajnta.geocoach.app.globals.busevents.UserLoginStatusEvent;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsDeviceManager;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsManager;
import pl.szmajnta.geocoach.app.gpsmanaging.events.SatellitesCountChanged;
import pl.szmajnta.geocoach.app.restapi.model.ApiUser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Fullscreen
@WindowFeature({ Window.FEATURE_NO_TITLE })
@EActivity(R.layout.layout_menu_activity)
public class MenuActivity extends FragmentActivity implements
        SplashFragment.FragmentInteractionListener{

    @ViewById(R.id.drawer_layout) DrawerLayout navigationDrawer;
    @ViewById TextView textViewProfileEmail;
    @ViewById TextView textViewProfileUsername;
    @ViewById CircleImageView imageViewProfile;
    @ViewById ListView listViewDrawerMenu;

    @Bean EventBus bus;
    @Bean(GpsDeviceManager.class) GpsManager gpsManager;

    @Pref GpsPreferences_ gpsPrefs;

    private boolean waitingForFix = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new SplashFragment_())
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Override
    public void onSplashTimer() {
        changeFragment(new MenuFragment_());
    }

    @AfterInject
    void registerToBus(){
        bus.register(this);
    }

    @AfterInject
    void warmGps(){
        gpsManager.searchForSatellites();
    }

    @AfterViews
    void checkUserLoggin(){
        enableNavigationDrawer(false);
    }

    @AfterViews
    void setDrawerListView(){
        List<String> menuLabels = new ArrayList<String>();
        menuLabels.add("");
        listViewDrawerMenu.setAdapter(new MenuListAdapter());
    }

    @UiThread
    void enableNavigationDrawer(boolean enable){
        if(enable){
            navigationDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else{
            navigationDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @UiThread
    void changeFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.abc_slide_in_bottom, R.anim.abc_fade_out)
                .replace(R.id.container, fragment)
                .commit();
    }

    @UiThread
    void changeFragmentWithoutAnimation(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @UiThread
    void setNavigationHeaderWithUser(ApiUser user){
        textViewProfileEmail.setText(user.getEmail());
        textViewProfileUsername.setText(user.getUsername());
        loadImageFromUrl(user.getGravatarUrl());
    }

    @UiThread
    void setImageSource(Bitmap image){
        imageViewProfile.setImageBitmap(image);
    }

    @Background
    void loadImageFromUrl(String source){
        try {
            URL url = new URL(source);
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            setImageSource(bmp);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onChangeSatellitesCount(SatellitesCountChanged event){
        if(!waitingForFix) {
            showToast("Found " +event.getCount() + " satellites");
        }
    }

    @Subscribe
    public void onChangeFragmentEvent(ChangeFragmentEvent event){
        if(event.isAnimationEnable()) {
            changeFragment(event.getRequestedFragment());
        } else{
            changeFragmentWithoutAnimation(event.getRequestedFragment());
        }
    }

    @Subscribe public void onUserLoggedStateChange(UserLoginStatusEvent event){
        enableNavigationDrawer(event.isLogged());
    }

    @Subscribe public void onUserInformationFetched(UserInformationFetchedEvent event){
        setNavigationHeaderWithUser(event.getUser());
    }

    @Subscribe public void onNewTrainingStartRequest(StartTrainingEvent event){
        waitingForFix = true;
    }

    @Subscribe public void onAccuracyChanged(float accuracy){
        if(waitingForFix){
            if(accuracy <= gpsPrefs.minAccuracy().get()) {
                Intent i = new Intent(this, MainTrackerActivity.class);
                startActivity(i);
            }
        }
    }


    @UiThread
    void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    class MenuListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return null;
        }
    }
}
