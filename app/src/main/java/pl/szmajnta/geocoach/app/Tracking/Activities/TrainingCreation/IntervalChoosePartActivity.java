package pl.szmajnta.geocoach.app.Tracking.Activities.TrainingCreation;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.Interval;
import pl.szmajnta.geocoach.app.Model.Filters.AllFilter;
import pl.szmajnta.geocoach.app.Model.Filters.FastPartFilter;
import pl.szmajnta.geocoach.app.Model.Filters.IntervalPartFilter;
import pl.szmajnta.geocoach.app.Model.Filters.SlowPartFilter;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.VibrateUtils;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-08-08.
 */
public class IntervalChoosePartActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> {

    BaseAdapter adapter;
    protected TrackerDatabaseHelper helper;
    private List<Interval> intervals;

    private ListView listViewParts;

    private int sorterCount = 0;

    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_choose_part);

        helper = getHelper();

        intervals = helper.getAllInterval();
        listViewParts = (ListView) findViewById(R.id.lvIntervalParts);

        updateListView(new AllFilter().sort(intervals));

        listViewParts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                VibrateUtils.VibrateShort(IntervalChoosePartActivity.this);
                Interval interval = (Interval)adapter.getItem(i);
                Intent intent = new Intent();
                intent.putExtra("result", interval.getId());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        ImageButton changeMode = (ImageButton)findViewById(R.id.change_mode_button);
        changeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(IntervalChoosePartActivity.this);
                IntervalPartFilter filter;
                sorterCount++;
                if(sorterCount > 2) sorterCount = 0;
                if(sorterCount == 0){
                    filter = new AllFilter();
                } else if(sorterCount == 1) {
                    filter = new SlowPartFilter();
                } else if(sorterCount == 2){
                    filter = new FastPartFilter();
                } else{
                    filter = new AllFilter();
                }

                updateListView(filter.sort(intervals));
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(IntervalChoosePartActivity.this);
                onBackPressed();
            }
        });
    }

    private void updateListView(List<Interval> list){
        adapter = new IntervalPartAdapter(list);
        listViewParts.setAdapter(adapter);
    }

    private class IntervalPartAdapter extends BaseAdapter {

        private List<Interval> intervalList;

        private IntervalPartAdapter(List<Interval> nodesTemplates) {
            this.intervalList = nodesTemplates;
        }

        @Override
        public int getCount() {
            return intervalList.size();
        }

        @Override
        public Object getItem(int i) {
            return intervalList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = IntervalChoosePartActivity.this.getLayoutInflater();
            View convertView = layoutInflater.inflate(R.layout.item_interv, viewGroup, false);

            Interval interv = intervalList.get(i);

            ImageView first = (ImageView) convertView.findViewById(R.id.firstLineImage);
            ImageView secondImage = (ImageView) convertView.findViewById(R.id.secondLineImage);
            if(interv.getDescription().equalsIgnoreCase("slow")) first.setImageDrawable(getResources().getDrawable(R.drawable.snail));
            else if(interv.getDescription().equalsIgnoreCase("fast")) first.setImageDrawable(getResources().getDrawable(R.drawable.runer));
            TextView second = (TextView) convertView.findViewById(R.id.secondLine);
            TextView third = (TextView) convertView.findViewById(R.id.thirdLine);

            if(interv.getType().getDisplayTitle().equalsIgnoreCase("dystans")) {
                secondImage.setImageDrawable(getResources().getDrawable(R.drawable.length));

                DecimalFormat formatter = new DecimalFormat("###.##");
                String sValue = "";
                float intervValue = interv.getValue();
                if (intervValue < 1000) {
                    sValue = formatter.format(intervValue) + "m";
                } else {
                    sValue = formatter.format(intervValue / 1000) + "km";
                }
                third.setText(String.valueOf(sValue));
            }
            second.setText(interv.getName());

            return convertView;
        }
    }
}