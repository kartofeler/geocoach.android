package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class Training {

    public final static String COLUMN_ID = "id";
    public final static String COLUMN_NAME = "name";
    public final static String COLUMN_DATE = "training_date";
    public final static String COLUMN_TYPE = "training_type";
    public final static String COLUMN_NODE_TRAIN_NAME = "orientation_route_name";
    public final static String COLUMN_NODES_COUNT = "nodes_count";
    public final static String COLUMN_STATISTIC = "statistic";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private int id;
    @DatabaseField(columnName = COLUMN_NAME)
    private String name;
    @DatabaseField(columnName = COLUMN_DATE)
    private Date date;
    @DatabaseField(columnName = COLUMN_TYPE, foreign = true, foreignAutoRefresh = true)
    private TrainingType type;
    @DatabaseField(columnName = COLUMN_NODE_TRAIN_NAME)
    private String orientation_route_name;
    @DatabaseField(columnName = COLUMN_NODES_COUNT)
    private int nodes_count;
    @DatabaseField(columnName = COLUMN_STATISTIC, foreign = true, foreignAutoRefresh = true)
    private Statistic statistic;

    public Training() {
    }

    public Training(String name, Date date, TrainingType type, String orientation_route_name, int nodes_count, Statistic statistic) {
        this.name = name;
        this.date = date;
        this.type = type;
        this.orientation_route_name = orientation_route_name;
        this.nodes_count = nodes_count;
        this.statistic = statistic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TrainingType getType() {
        return type;
    }

    public void setType(TrainingType type) {
        this.type = type;
    }

    public String getOrientation_route_name() {
        return orientation_route_name;
    }

    public void setOrientation_route_name(String orientation_route_name) {
        this.orientation_route_name = orientation_route_name;
    }

    public int getNodes_count() {
        return nodes_count;
    }

    public void setNodes_count(int nodes_count) {
        this.nodes_count = nodes_count;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public void setStatistic(Statistic statistic) {
        this.statistic = statistic;
    }
}
