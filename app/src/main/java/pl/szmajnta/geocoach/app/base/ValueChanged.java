package pl.szmajnta.geocoach.app.base;

/**
 * Created by andrzej on 18.05.15.
 */
public class ValueChanged<T> {
    protected T count;

    public ValueChanged(T count) {
        this.count = count;
    }

    public T getCount() {
        return count;
    }
}
