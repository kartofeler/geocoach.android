package pl.szmajnta.geocoach.app.gpsmanaging;

import android.content.Context;
import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.sharedpreferences.Pref;

import pl.szmajnta.geocoach.app.globals.EventBus;
import pl.szmajnta.geocoach.app.base.ValueChanged;
import pl.szmajnta.geocoach.app.globals.GpsPreferences_;
import pl.szmajnta.geocoach.app.gpsmanaging.events.AccuracyChanged;
import pl.szmajnta.geocoach.app.gpsmanaging.events.FirstFixEvent;
import pl.szmajnta.geocoach.app.gpsmanaging.events.LocationChanged;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Andrzej Laptop on 2014-05-07.
 */
@EBean(scope = EBean.Scope.Singleton)
public class GpsDeviceManager implements GpsManager, GpsStatus.Listener, GpsStatus.NmeaListener, LocationListener {

    @RootContext Context context;
    @SystemService LocationManager locationManager;

    @Bean EventBus bus;
    @Pref GpsPreferences_ gpsPrefs;

    private Location lastLocation;
    private GpsStatus lastStatus;

    private boolean isRun = false;
    private int satellitesCount = 0;

    private Timer gpsTimer;
    private Handler handler;

    public GpsDeviceManager() {
        handler = new Handler();
    }

    @AfterInject
    void registerToBus(){
        bus.register(this);
    }

    @Override
    public void searchForSatellites() {
        if (!isRun) {
            Log.d("GeocoachGPS", "GPS started");
            locationManager.addGpsStatusListener(this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            isRun = true;
        }
    }

    @Override
    public void startGps() {
        Log.d("GeocoachGPS", "GPS started");
        gpsTimer = new Timer();
        int syncConnPref = gpsPrefs.gpsTime().get();
        gpsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, GpsDeviceManager.this , null);
                    }
                });
            }
        }, 100, syncConnPref*1000);
    }

    @Override
    public void stopGps() {
        Log.d("GeocoachGPS", "GPS stopped");
        locationManager.removeGpsStatusListener(this);
        locationManager.removeUpdates(this);
        isRun = false;
    }

    @Override
    public void onGpsStatusChanged(int event) {
        if (lastStatus == null) {
            lastStatus = locationManager.getGpsStatus(null);
        } else {
            lastStatus = locationManager.getGpsStatus(lastStatus);
        }

        if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            int newCount = gpsSatellitesCount(lastStatus);
            if(satellitesCount != newCount) {
                Log.d("GeocoachGPS", "Satellites count: " + satellitesCount);
                satellitesCount = newCount;
                satellitesCountChanged(satellitesCount);
            }
        }
        if (event == GpsStatus.GPS_EVENT_FIRST_FIX) {
            firstFix();
        }
    }

    private int gpsSatellitesCount(GpsStatus status){
        int satellitesInFix = 0;
        for (GpsSatellite sat : status.getSatellites()) {
            satellitesInFix++;
        }
        return satellitesInFix;
    }

    @Override
    public void onNmeaReceived(long l, String s) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (lastLocation == null) lastLocation = location;
        else {
            if (lastLocation.getAccuracy() != location.getAccuracy()) {
                float acc = location.getAccuracy();
                accuracyChanged(acc);
            }
        }
        locationChanged(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Background
    public void satellitesCountChanged(int count) {
        bus.post(new ValueChanged(count));
    }

    @Background
    public void firstFix() {
        bus.post(new FirstFixEvent());
    }

    @Background
    public void accuracyChanged(float accuracy) {
        bus.post(new AccuracyChanged(accuracy));
    }

    @Background
    public void locationChanged(Location location) {
        bus.post(new LocationChanged(location));
    }
}
