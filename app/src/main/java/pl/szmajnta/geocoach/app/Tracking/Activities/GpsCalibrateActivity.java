package pl.szmajnta.geocoach.app.Tracking.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsStatusListener;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsDeviceManager;
import pl.szmajnta.geocoach.app.Model.RouteSettings;
import pl.szmajnta.geocoach.app.R;

import java.text.DecimalFormat;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class GpsCalibrateActivity extends Activity implements GpsStatusListener {
    private GpsDeviceManager gpsManager;
    private RouteSettings settings;

    public void onCreate(Bundle savedInstanceState) {
        Intent i = getIntent();
        final boolean xperia = i.getBooleanExtra("IsXperia", false);
        //settings = i.getParcelableExtra("RouteSetting");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_gps_calibrate);

//        gpsManager = new GpsDeviceManager(this);
//        gpsManager.addGpsStatusObserver(this);

        gpsManager.searchForSatellites();

        findViewById(R.id.buttonStart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GpsCalibrateActivity.this, MainTrackerActivity.class);
                startActivity(intent);
            }
        });
    }




    @Override
    public void OnSatellitesCountChanged(int count) {
        TextView textViewSatellites = (TextView)findViewById(R.id.tvCurrentSatellites);
        textViewSatellites.setText(String.valueOf(count));
    }

    @Override
    public void OnFirstFix() {
        TextView accuracy = (TextView)findViewById(R.id.tvAccuracy);
        accuracy.setTextColor(Color.GREEN);

        TextView accuracyText = (TextView)findViewById(R.id.tvAccuracyText);
        accuracyText.setTextColor(Color.GREEN);
    }

    @Override
    public void OnAccuracyChanged(float accuracy) {
        boolean enableButton = true;
        TextView accuracyText = (TextView) findViewById(R.id.tvAccuracy);
        if(accuracy <= 100) {
            String decimalAccuracy = new DecimalFormat("#.#").format(accuracy);
            accuracyText.setText(String.valueOf(decimalAccuracy));

            enableButton = true;
        } else{
            accuracyText.setText("---");
        }

        Button startButton = (Button)findViewById(R.id.buttonStart);
        startButton.setEnabled(enableButton);
    }

    @Override
    public void OnLocationChanged(Location location) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        gpsManager.searchForSatellites();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        gpsManager.removeGpsStatusObserver(this);
        gpsManager.stopGps();
    }
}