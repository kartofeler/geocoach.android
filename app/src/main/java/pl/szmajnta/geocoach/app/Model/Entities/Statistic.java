package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class Statistic {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private float maxSpeed;
    @DatabaseField
    private float avgSpeed;
    @DatabaseField
    private float maxPace;
    @DatabaseField
    private float avgPace;
    @DatabaseField
    private float maxAltitude;
    @DatabaseField
    private float minAltitude;
    @DatabaseField
    private float totalDistance;
    @DatabaseField
    private int totalTime;
    @DatabaseField
    private float maxSlope;

    public Statistic() {
    }

    public Statistic(float maxSpeed, float avgSpeed,
                     float maxPace, float avgPace,
                     float maxAltitude, float minAltitude,
                     float totalDistance, int totalTime,
                     float maxSlope) {
        this.maxSpeed = maxSpeed;
        this.avgSpeed = avgSpeed;
        this.maxPace = maxPace;
        this.avgPace = avgPace;
        this.maxAltitude = maxAltitude;
        this.minAltitude = minAltitude;
        this.totalDistance = totalDistance;
        this.totalTime = totalTime;
        this.maxSlope = maxSlope;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(float avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public float getMaxPace() {
        return maxPace;
    }

    public void setMaxPace(float maxPace) {
        this.maxPace = maxPace;
    }

    public float getAvgPace() {
        return avgPace;
    }

    public void setAvgPace(float avgPace) {
        this.avgPace = avgPace;
    }

    public float getMaxAltitude() {
        return maxAltitude;
    }

    public void setMaxAltitude(float maxAltitude) {
        this.maxAltitude = maxAltitude;
    }

    public float getMinAltitude() {
        return minAltitude;
    }

    public void setMinAltitude(float minAltitude) {
        this.minAltitude = minAltitude;
    }

    public float getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(float totalDistance) {
        this.totalDistance = totalDistance;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public float getMaxSlope() {
        return maxSlope;
    }

    public void setMaxSlope(float maxSlope) {
        this.maxSlope = maxSlope;
    }
}
