package pl.szmajnta.geocoach.app.Export.Exceptions;

/**
 * Created by The Ja on 2014-06-08.
 */
public class ExportException extends Exception {
    public ExportException(){super();}
    public ExportException(String message){super(message);}
}
