package pl.szmajnta.geocoach.app.restapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrzej on 2015-04-13.
 */
public class AccessToken {
    @JsonProperty("access_token")
    private String accessToken;

    public AccessToken() {
    }

    public AccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    @JsonProperty("access_token")
    public String getAccessToken() {
        return accessToken;
    }
    @JsonProperty("access_token")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
