package pl.szmajnta.geocoach.app.Interfaces;

import android.location.Location;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-23.
 */
public interface NodeManagerInterface {
    public int getNodesCount();
    public TrainingNode getCurrentNode();
    public TrainingNode getNextNode();
    public TrainingNode getPreviousNode();
    public TrainingNode switchToNextNode();
    public int getCurrentNodeIndex();
    public boolean checkSwitchPossible(Location location);
    public boolean checkSwitchPossible(float latitude, float longitude);
    public List<TrainingNode> getAllNodes();

}
