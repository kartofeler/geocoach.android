package pl.szmajnta.geocoach.app.globals;

/**
 * Created by Andrzej on 2015-04-17.
 */
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import org.androidannotations.annotations.EBean;

@EBean(scope = EBean.Scope.Singleton)
public class EventBus extends Bus{
    private static final Bus bus = new Bus(ThreadEnforcer.ANY);
    public static Bus bus() {
        return bus;
    }

    @Override
    public void register(Object object) {
        bus.register(object);
    }

    @Override
    public void unregister(Object object) {
        bus.unregister(object);
    }

    @Override
    public void post(Object event) {
        bus.post(event);
    }
}
