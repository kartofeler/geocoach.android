package pl.szmajnta.geocoach.app.restapi.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrzej on 2015-04-13.
 */
public class RegisterData extends LoginData {
    @JsonProperty()
    private String username;

    public RegisterData(String email, String password, String username) {
        super(email, password);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
