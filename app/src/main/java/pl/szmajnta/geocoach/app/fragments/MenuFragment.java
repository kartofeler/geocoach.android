package pl.szmajnta.geocoach.app.fragments;

import android.app.Activity;
import android.app.usage.UsageEvents;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.daimajia.easing.linear.Linear;
import com.squareup.otto.Subscribe;
import org.androidannotations.annotations.*;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.beans.GeocoachUser;
import pl.szmajnta.geocoach.app.globals.EventBus;
import pl.szmajnta.geocoach.app.globals.busevents.ChangeFragmentEvent;
import pl.szmajnta.geocoach.app.globals.busevents.StartTrainingEvent;
import pl.szmajnta.geocoach.app.globals.busevents.UserLoginStatusEvent;

@EFragment(R.layout.fragment_menu)
public class MenuFragment extends Fragment {

    @Bean GeocoachUser user;
    @Bean EventBus bus;

    @ViewById LinearLayout layoutLogginLink;
    @ViewById ImageButton buttonNewRoute;

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        return fragment;
    }

    public MenuFragment() {

    }

    @AfterViews
    void updateView(){
        bus.register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        bus.unregister(this);
    }

    @UiThread
    void showLogginIcon(boolean show){
        if(show) {
            layoutLogginLink.setVisibility(View.VISIBLE);
        } else{
            layoutLogginLink.setVisibility(View.INVISIBLE);
        }
    }

    @Click(R.id.buttonGoLogin)
    void buttonGoLogin(){
        bus.post(new ChangeFragmentEvent(new LoginFragment_()));
    }

    @Click(R.id.buttonNewRoute)
    void startNewTrainingActivity(){
        bus.post(new ChangeFragmentEvent(new WaitForFixFragment_()));
        bus.post(new StartTrainingEvent());
    }

    @Subscribe public void onUserLoggedStateChange(UserLoginStatusEvent event){
        showLogginIcon(!event.isLogged());
    }
}
