package pl.szmajnta.geocoach.app.gpsmanaging.events;

import android.location.Location;

import pl.szmajnta.geocoach.app.base.ValueChanged;

/**
 * Created by andrzej on 18.05.15.
 */
public class LocationChanged extends ValueChanged<Location> {
    public LocationChanged(Location count) {
        super(count);
    }
}
