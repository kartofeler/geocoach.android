package pl.szmajnta.geocoach.app.globals.busevents;

import pl.szmajnta.geocoach.app.restapi.model.ApiUser;

/**
 * Created by Andrzej on 2015-04-19.
 */
public class UserInformationFetchedEvent {
    private ApiUser user;

    public UserInformationFetchedEvent(ApiUser logged) {
        this.user = logged;
    }

    public ApiUser getUser() {
        return user;
    }

    public void setUser(ApiUser user) {
        this.user = user;
    }
}
