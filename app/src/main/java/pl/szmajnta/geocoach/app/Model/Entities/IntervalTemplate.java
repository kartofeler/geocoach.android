package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class IntervalTemplate {

    public final static String COLUMN_ID = "id";
    public final static String COLUMN_NAME = "name";
    public final static String COLUMN_DESCRIPTION = "description";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private int id;
    @DatabaseField(columnName = COLUMN_NAME)
    private String name;
    @DatabaseField(columnName = COLUMN_DESCRIPTION)
    private String description;

    public IntervalTemplate() {
    }

    public IntervalTemplate(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
