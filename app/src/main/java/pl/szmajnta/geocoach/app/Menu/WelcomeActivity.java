package pl.szmajnta.geocoach.app.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import pl.szmajnta.geocoach.app.R;

/**
 * Created by Andrzej Laptop on 2014-05-29.
 */
public class WelcomeActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(WelcomeActivity.this, SplashActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, 5000);
    }
}