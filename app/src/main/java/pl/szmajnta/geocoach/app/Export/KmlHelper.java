package pl.szmajnta.geocoach.app.Export;

import pl.szmajnta.geocoach.app.Export.Exceptions.KmlException;
import pl.szmajnta.geocoach.app.Model.Entities.GpsSample;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by The Ja on 2014-06-07.
 */
public class KmlHelper implements RouteExporter {

    public KmlHelper(){}

    private TrainingRoute Route;
    private String LineColor = "ff0000e6";
    private int LineWidth = 4;

    public String getLineColor() {
        return LineColor;
    }

    public void setLineColor(String lineColor) {
        if(lineColor.length() != 8)
            return;
        if(lineColor.matches("[0-9a-f]+"))
            LineColor = lineColor;
    }

    public int getLineWidth() {
        return LineWidth;
    }

    public void setLineWidth(int lineWidth) {
        if(lineWidth <= 0)
            return;
        LineWidth = lineWidth;
    }

    @Override
    public void setRoute(TrainingRoute route) {
        Route = route;
    }

    @Override
    public TrainingRoute getRoute() {
        return Route;
    }

    @Override
    public String Export() throws KmlException {
        if(Route == null)
            throw new KmlException("Route not set!");
        if(Route.getSamples().size() == 0)
            throw new KmlException("The route is empty!");

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();

            // xml node - root node
            Element rootElement = doc.createElement("kml");
            rootElement.setAttribute("xmlns", "http://earth.google.com/kml/2.2");
            doc.appendChild(rootElement);

            // Document node
            Element documentNode = doc.createElement("Document");
            rootElement.appendChild(documentNode);

            // Name node
            Element nameNode = doc.createElement("name");
            nameNode.setTextContent(Route.getName());
            documentNode.appendChild(nameNode);

            // Placemarks
            Element placemarksFolderNode = doc.createElement("Folder");
            placemarksFolderNode.setAttribute("id", "Waypoints");

            for (Placemark placemark : Route.getPlacemarks()) {
                Element placemarkNode = CreatePlacemarkNode(doc, placemark);
                placemarksFolderNode.appendChild(placemarkNode);
            }
            documentNode.appendChild(placemarksFolderNode);

            // Path
            Element pathFolderNode = CreatePathNode(doc, Route.getSamples());
            documentNode.appendChild(pathFolderNode);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            StringWriter writer = new StringWriter();
            transformer.transform(source, new StreamResult(writer));

            return writer.toString();
        }
        catch (Exception e) {
            throw (new KmlException(e.getMessage()));
        }
    }

    private Element CreatePathNode(Document document, List<GpsSample> samples){
        Element pathFolderNode = document.createElement("Folder");
        pathFolderNode.setAttribute("id", "Tracks");

        Element placemarkNode = document.createElement("Placemark");

        // Line style
        Element styleNode = document.createElement("Style");
        Element lineStyleNode = document.createElement("LineStyle");
        // Line color
        Element colorNode = document.createElement("color");
        colorNode.setTextContent(LineColor);
        lineStyleNode.appendChild(colorNode);
        // Line width
        Element widthNode = document.createElement("width");
        widthNode.setTextContent(Integer.toString(LineWidth));
        lineStyleNode.appendChild(widthNode);
        styleNode.appendChild(lineStyleNode);
        placemarkNode.appendChild(styleNode);

        // Points
        Element multigeometryNode = document.createElement("MultiGeometry");
        Element lineStringNode = document.createElement("LineString");
        // Tesselate
        Element tesselateNode = document.createElement("tesselate");
        tesselateNode.setTextContent("1");
        lineStringNode.appendChild(tesselateNode);
        // Mode
        Element modeNode = document.createElement("altitudeMode");
        modeNode.setTextContent("clampToGround");
        lineStringNode.appendChild(modeNode);
        // Coordinates
        Element coordinatesNode = document.createElement("coordinates");
        String coordinates = "";
        for(GpsSample sample : samples){
            coordinates += sample.getLatitude() + "," + sample.getLongitude() + " ";
        }
        coordinatesNode.setTextContent(coordinates);

        lineStringNode.appendChild(coordinatesNode);
        multigeometryNode.appendChild(lineStringNode);
        placemarkNode.appendChild(multigeometryNode);
        pathFolderNode.appendChild(placemarkNode);

        return pathFolderNode;
    }

    private Element CreatePlacemarkNode(Document document, Placemark placemark){
        Element placemarkNode = document.createElement("Placemark");

        // Set name
        Element nameNode = document.createElement("name");
        nameNode.setTextContent(placemark.getName());
        placemarkNode.appendChild(nameNode);

        // Point node
        Element point = document.createElement("Point");
        // Mode
        Element modeNode = document.createElement("altitudeMode");
        modeNode.setTextContent("clampToGround");
        point.appendChild(modeNode);
        // Coordinates
        Element coordsNode = document.createElement("coordinates");
        coordsNode.setTextContent(placemark.getPosition().getLatitude() + ", " + placemark.getPosition().getLongitude());
        point.appendChild(coordsNode);

        placemarkNode.appendChild(point);

        return placemarkNode;
    }
}
