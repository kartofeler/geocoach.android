package pl.szmajnta.geocoach.app.Tracking.Activities.TrainingCreation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.Interval;
import pl.szmajnta.geocoach.app.Model.Entities.IntervalTemplate;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.VibrateUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by The Ja on 2014-05-29.
 */
public class IntervalTemplateAddActivity extends Activity {

    private final static int REQUEST_CODE_FOR_CHOOSE_PART = 1101;
    private List<Interval> intervalList;
    private TrackerDatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_create_training);

        helper = new TrackerDatabaseHelper(this);
        intervalList = new ArrayList<Interval>();

        ImageButton addButton = (ImageButton)findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(IntervalTemplateAddActivity.this);
                Intent intent = new Intent(IntervalTemplateAddActivity.this, IntervalChoosePartActivity.class);
                startActivityForResult(intent, REQUEST_CODE_FOR_CHOOSE_PART);
            }
        });

        ImageButton trashButton = (ImageButton)findViewById(R.id.trash_button);
        trashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VibrateUtils.VibrateShort(IntervalTemplateAddActivity.this);
                intervalList.clear();
                updateListView();
            }
        });

        ImageButton saveButton = (ImageButton)findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(IntervalTemplateAddActivity.this);
                EditText nameEdit = (EditText)findViewById(R.id.etTemplateName);
                String name = nameEdit.getText().toString();

                IntervalTemplate template = new IntervalTemplate(name, "");
                helper.addIntervalTemplate(template);


                for(Interval interval : intervalList){
                    helper.addIntervalToTemplate(template, interval);
                }

                onBackPressed();
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(IntervalTemplateAddActivity.this);
                onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_FOR_CHOOSE_PART){
            if(resultCode == RESULT_OK) {
                int intervalId = data.getIntExtra("result", -1);
                if (intervalId != -1) {
                    Interval interval = helper.getIntervalById(intervalId);
                    intervalList.add(interval);
                    updateListView();
                }
            }
        }
    }

    private void updateListView(){
        ListView choosenIntervalList = (ListView)findViewById(R.id.lvChoosenPart);
        BaseAdapter adapter = new IntervalPartAdapter(intervalList);
        choosenIntervalList.setAdapter(adapter);
    }

    private class IntervalPartAdapter extends BaseAdapter {

        private List<Interval> intervalList;

        private IntervalPartAdapter(List<Interval> nodesTemplates) {
            this.intervalList = nodesTemplates;
        }

        @Override
        public int getCount() {
            return intervalList.size();
        }

        @Override
        public Object getItem(int i) {
            return intervalList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = IntervalTemplateAddActivity.this.getLayoutInflater();
            View convertView = layoutInflater.inflate(R.layout.item_interv, viewGroup, false);

            Interval interv = intervalList.get(i);

            ImageView first = (ImageView) convertView.findViewById(R.id.firstLineImage);
            ImageView secondImage = (ImageView) convertView.findViewById(R.id.secondLineImage);
            
            TextView second = (TextView) convertView.findViewById(R.id.secondLine);
            TextView third = (TextView) convertView.findViewById(R.id.thirdLine);

            if(interv.getDescription().equalsIgnoreCase("slow")) first.setImageDrawable(getResources().getDrawable(R.drawable.snail));
            else if(interv.getDescription().equalsIgnoreCase("fast")) first.setImageDrawable(getResources().getDrawable(R.drawable.runer));

            if(interv.getType().getDisplayTitle().equalsIgnoreCase("dystans")) {
                secondImage.setImageDrawable(getResources().getDrawable(R.drawable.length));

                DecimalFormat formatter = new DecimalFormat("###.##");
                String sValue = "";
                float intervValue = interv.getValue();
                if (intervValue < 1000) {
                    sValue = formatter.format(intervValue) + "m";
                } else {
                    sValue = formatter.format(intervValue / 1000) + "km";
                }
                third.setText(String.valueOf(sValue));
            }
            second.setText(interv.getName());

            return convertView;
        }
    }
}
