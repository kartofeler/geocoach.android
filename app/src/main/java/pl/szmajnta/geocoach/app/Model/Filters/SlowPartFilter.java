package pl.szmajnta.geocoach.app.Model.Filters;

import pl.szmajnta.geocoach.app.Model.Entities.Interval;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-08-09.
 */
public class SlowPartFilter implements IntervalPartFilter{
    @Override
    public List<Interval> sort(List<Interval> list) {
        List<Interval> newList = new ArrayList<Interval>();
        for(Interval interval : list){
            if(interval.getDescription().equalsIgnoreCase("slow")){
                newList.add(interval);
            }
        }
        return newList;
    }
}
