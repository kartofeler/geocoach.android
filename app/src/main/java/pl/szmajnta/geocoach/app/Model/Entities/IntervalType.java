package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class IntervalType {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String displayTitle;
    @DatabaseField
    private String description;

    public IntervalType() {
    }

    public IntervalType(String displayTitle, String description) {
        this.displayTitle = displayTitle;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
