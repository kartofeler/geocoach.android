package pl.szmajnta.geocoach.app.Model.DatabaseAccess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import pl.szmajnta.geocoach.app.Model.Entities.*;
import pl.szmajnta.geocoach.app.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-30.
 */
public class TrackerDatabaseHelper extends OrmLiteSqliteOpenHelper implements TrackerDataRepository {

    private static final String DATABASE_NAME = "geocoach.db";
    private static final int DATABASE_VERSION = 11;

    private RuntimeExceptionDao<TrainingNode, Integer> nodeRuntimeDao = null;
    private RuntimeExceptionDao<NodesTemplate, Integer> nodeTemplateRuntimeDao = null;
    private RuntimeExceptionDao<NodeForTemplateJoin, Integer> nodeTemplateJoinRuntimeDao = null;
    private RuntimeExceptionDao<TrainingType, Integer> trainingTypeRuntimeDao = null;
    private RuntimeExceptionDao<Training, Integer> trainingRuntimeDao = null;
    private RuntimeExceptionDao<GpsSample, Integer> gpsSampleRuntimeDao = null;
    private RuntimeExceptionDao<Statistic, Integer> statisticRuntimeDao = null;
    private RuntimeExceptionDao<Interval, Integer> intervalRuntimeDao = null;
    private RuntimeExceptionDao<IntervalForTemplateJoin, Integer> intervalForTemplateRuntimeDao = null;
    private RuntimeExceptionDao<IntervalForTrainingJoin, Integer> intervalForTrainingRuntimeDao = null;
    private RuntimeExceptionDao<IntervalTemplate, Integer> intervalTemplatesRuntimeDao = null;
    private RuntimeExceptionDao<IntervalType, Integer> intervalTypeRuntimeDao = null;


    public TrackerDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, TrainingNode.class);
            TableUtils.createTable(connectionSource, NodesTemplate.class);
            TableUtils.createTable(connectionSource, NodeForTemplateJoin.class);
            TableUtils.createTable(connectionSource, TrainingType.class);

            TableUtils.createTable(connectionSource, Statistic.class);
            TableUtils.createTable(connectionSource, Training.class);
            TableUtils.createTable(connectionSource, GpsSample.class);

            TableUtils.createTable(connectionSource, IntervalType.class);
            TableUtils.createTable(connectionSource, Interval.class);
            TableUtils.createTable(connectionSource, IntervalTemplate.class);
            TableUtils.createTable(connectionSource, IntervalForTemplateJoin.class);
            TableUtils.createTable(connectionSource, IntervalForTrainingJoin.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Wszystko co trzeba dodawać, takze w celach testowych, wyrzucać do prywatnych metod, żeby nie
        // powstała boska metoda, niech metody się kończą na Records

        addTrainingTypesRecords();
        addIntervalTypes();
        addIntervalPartRecords();
    }

    private void addTrainingTypesRecords(){
        getTrainingTypeRuntimeDao().create(new TrainingType("Normalny", "Biegnij przed siebie nie zważając na nic"));
        getTrainingTypeRuntimeDao().create(new TrainingType("Na orientacje", "Odwiedź wyznaczone punkty geograficzne"));
        getTrainingTypeRuntimeDao().create(new TrainingType("Interwałowy", "Biegnij w etapach narzucanych przez plan"));
        getTrainingTypeRuntimeDao().create(new TrainingType("Interwałowo-na orientację", "Biegnij w etapach interwałowych po wyznaczonych punktach"));
    }

    private void addIntervalTypes(){
        getIntervalTypeRuntimeDao().create(new IntervalType("Dystans", "Biegnij odpowiednią odległość"));
        getIntervalTypeRuntimeDao().create(new IntervalType("Czas", "Biegnij odpowiedni czas"));
    }

    private void addIntervalPartRecords(){
        List<IntervalType> types = getIntervalTypeRuntimeDao().queryForAll();
        getIntervalRuntimeDao().create(new Interval("Szybki i krótki!", "Fast", types.get(0), 100));
        getIntervalRuntimeDao().create(new Interval("Szybki i krótki!", "Fast", types.get(0), 200));
        getIntervalRuntimeDao().create(new Interval("Szybki i średni!", "Fast", types.get(0), 500));
        getIntervalRuntimeDao().create(new Interval("Szybki i długi!", "Fast", types.get(0), 1000));
        getIntervalRuntimeDao().create(new Interval("Szybki i długi!", "Fast", types.get(0), 2000));
        getIntervalRuntimeDao().create(new Interval("Szybki i długi!", "Fast", types.get(0), 5000));
        getIntervalRuntimeDao().create(new Interval("Szybki i długi!", "Fast", types.get(0), 7500));


        getIntervalRuntimeDao().create(new Interval("Wolny i krótki!", "Slow", types.get(0), 100));
        getIntervalRuntimeDao().create(new Interval("Wolny i krótki!", "Slow", types.get(0), 200));
        getIntervalRuntimeDao().create(new Interval("Wolny i średni!", "Slow", types.get(0), 500));
        getIntervalRuntimeDao().create(new Interval("Wolny i długi!", "Slow", types.get(0), 1000));
        getIntervalRuntimeDao().create(new Interval("Wolny i długi!", "Slow", types.get(0), 2000));
        getIntervalRuntimeDao().create(new Interval("Wolny i długi!", "Slow", types.get(0), 5000));
        getIntervalRuntimeDao().create(new Interval("Wolny i długi!", "Slow", types.get(0), 7500));
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        try {
            TableUtils.dropTable(connectionSource, TrainingNode.class, true);
            TableUtils.dropTable(connectionSource, NodesTemplate.class, true);
            TableUtils.dropTable(connectionSource, NodeForTemplateJoin.class, true);
            TableUtils.dropTable(connectionSource, TrainingType.class, true);
            TableUtils.dropTable(connectionSource, Statistic.class, true);
            TableUtils.dropTable(connectionSource, Training.class, true);
            TableUtils.dropTable(connectionSource, GpsSample.class, true);

            TableUtils.dropTable(connectionSource, IntervalType.class, true);
            TableUtils.dropTable(connectionSource, Interval.class, true);
            TableUtils.dropTable(connectionSource, IntervalTemplate.class, true);
            TableUtils.dropTable(connectionSource, IntervalForTemplateJoin.class, true);
            TableUtils.dropTable(connectionSource, IntervalForTrainingJoin.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private PreparedQuery<TrainingNode> nodeForTemplateQuery = null;
    private PreparedQuery<Interval> intervalForTemplateQuery = null;

    private RuntimeExceptionDao<TrainingNode, Integer> getNodeRuntimeDao() {
        if(nodeRuntimeDao == null){
            nodeRuntimeDao = getRuntimeExceptionDao(TrainingNode.class);
        }
        return nodeRuntimeDao;
    }

    private RuntimeExceptionDao<NodesTemplate, Integer> getNodeTemplateRuntimeDao() {
        if(nodeTemplateRuntimeDao == null){
            nodeTemplateRuntimeDao = getRuntimeExceptionDao(NodesTemplate.class);
        }
        return nodeTemplateRuntimeDao;
    }

    private RuntimeExceptionDao<NodeForTemplateJoin, Integer> getNodeTemplateJoinRuntimeDao() {
        if(nodeTemplateJoinRuntimeDao == null){
            nodeTemplateJoinRuntimeDao = getRuntimeExceptionDao(NodeForTemplateJoin.class);
        }
        return nodeTemplateJoinRuntimeDao;
    }

    private RuntimeExceptionDao<TrainingType, Integer> getTrainingTypeRuntimeDao() {
        if(trainingTypeRuntimeDao == null){
            trainingTypeRuntimeDao = getRuntimeExceptionDao(TrainingType.class);
        }
        return trainingTypeRuntimeDao;
    }

    private RuntimeExceptionDao<Training, Integer> getTrainingRuntimeDao() {
        if(trainingRuntimeDao == null){
            trainingRuntimeDao = getRuntimeExceptionDao(Training.class);
        }
        return trainingRuntimeDao;
    }

    private RuntimeExceptionDao<GpsSample, Integer> getGpsSampleRuntimeDao() {
        if(gpsSampleRuntimeDao == null){
            gpsSampleRuntimeDao = getRuntimeExceptionDao(GpsSample.class);
        }
        return gpsSampleRuntimeDao;
    }

    private RuntimeExceptionDao<Statistic, Integer> getStatisticRuntimeDao() {
        if(statisticRuntimeDao == null){
            statisticRuntimeDao = getRuntimeExceptionDao(Statistic.class);
        }
        return statisticRuntimeDao;
    }

    private RuntimeExceptionDao<Interval, Integer> getIntervalRuntimeDao() {
        if(intervalRuntimeDao == null){
            intervalRuntimeDao = getRuntimeExceptionDao(Interval.class);
        }
        return intervalRuntimeDao;
    }

    private RuntimeExceptionDao<IntervalForTemplateJoin, Integer> getIntervalForTemplateRuntimeDao() {
        if(intervalForTemplateRuntimeDao == null){
            intervalForTemplateRuntimeDao = getRuntimeExceptionDao(IntervalForTemplateJoin.class);
        }
        return intervalForTemplateRuntimeDao;
    }

    private RuntimeExceptionDao<IntervalForTrainingJoin, Integer> getIntervalForTrainingRuntimeDao() {
        if(intervalForTrainingRuntimeDao == null){
            intervalForTrainingRuntimeDao = getRuntimeExceptionDao(IntervalForTrainingJoin.class);
        }
        return intervalForTrainingRuntimeDao;
    }

    private RuntimeExceptionDao<IntervalTemplate, Integer> getIntervalTemplatesRuntimeDao() {
        if(intervalTemplatesRuntimeDao == null){
            intervalTemplatesRuntimeDao = getRuntimeExceptionDao(IntervalTemplate.class);
        }
        return intervalTemplatesRuntimeDao;
    }

    private RuntimeExceptionDao<IntervalType, Integer> getIntervalTypeRuntimeDao() {
        if(intervalTypeRuntimeDao == null){
            intervalTypeRuntimeDao = getRuntimeExceptionDao(IntervalType.class);
        }
        return intervalTypeRuntimeDao;
    }

    private List<TrainingNode> lookupTrainingNodesForTemplate(NodesTemplate template) throws SQLException {
        if (nodeForTemplateQuery == null) {
            nodeForTemplateQuery = makeTrainingForTemplateQuery();
        }
        nodeForTemplateQuery.setArgumentHolderValue(0, template);
        return nodeRuntimeDao.query(nodeForTemplateQuery);
    }



    private PreparedQuery<TrainingNode> makeTrainingForTemplateQuery() throws SQLException {
        // build our inner query for UserPost objects
        QueryBuilder<NodeForTemplateJoin, Integer> nodeTemplateQb = getNodeTemplateJoinRuntimeDao().queryBuilder();
        // just select the post-id field
        nodeTemplateQb.selectColumns(NodeForTemplateJoin.COLUMN_NODE_ID);
        SelectArg userSelectArg = new SelectArg();
        // you could also just pass in user1 here
        nodeTemplateQb.where().eq(NodeForTemplateJoin.COLUMN_TEMPLATE_ID, userSelectArg);

        // build our outer query for Post objects
        QueryBuilder<TrainingNode, Integer> postQb = getNodeRuntimeDao().queryBuilder();
        // where the id matches in the post-id from the inner query
        postQb.where().in(TrainingNode.COLUMN_ID , nodeTemplateQb);
        return postQb.prepare();
    }

    private List<Interval> lookupIntervalForTemplate(IntervalTemplate template) throws SQLException {
        if (intervalForTemplateQuery == null) {
            intervalForTemplateQuery = makeIntervalForTemplateQuery();
        }
        intervalForTemplateQuery.setArgumentHolderValue(0, template);
        return intervalRuntimeDao.query(intervalForTemplateQuery);
    }



    private PreparedQuery<Interval> makeIntervalForTemplateQuery() throws SQLException {
        // build our inner query for UserPost objects
        QueryBuilder<IntervalForTemplateJoin, Integer> nodeTemplateQb = getIntervalForTemplateRuntimeDao().queryBuilder();
        // just select the post-id field
        nodeTemplateQb.selectColumns(IntervalForTemplateJoin.COLUMN_INTERVAL_ID);
        SelectArg userSelectArg = new SelectArg();
        // you could also just pass in user1 here
        nodeTemplateQb.where().eq(IntervalForTemplateJoin.COLUMN_TEMPLATE_ID, userSelectArg);

        // build our outer query for Post objects
        QueryBuilder<Interval, Integer> postQb = getIntervalRuntimeDao().queryBuilder();
        // where the id matches in the post-id from the inner query
        postQb.where().in(TrainingNode.COLUMN_ID , nodeTemplateQb);
        return postQb.prepare();
    }

    @Override
    public List<Training> getAllTraining() {
        return getTrainingRuntimeDao().queryForAll();
    }

    @Override
    public Training getTrainingById(int id) {
        return getTrainingRuntimeDao().queryForId(id);
    }

    @Override
    public List<TrainingType> getAllTrainingType() {
        return getTrainingTypeRuntimeDao().queryForAll();
    }

    @Override
    public List<GpsSample> getAllSampleForTraining(Training training) {
        List<GpsSample> allSamples = getGpsSampleRuntimeDao().queryForAll();
        List<GpsSample> selectedSamples = new ArrayList<GpsSample>();
        for(GpsSample sample : allSamples){
            if(sample.getTraining().getId() == training.getId()){
                selectedSamples.add(sample);
            }
        }

        return selectedSamples;
    }


    @Override
    public List<Interval> getIntervalsForTraining(Training training) {
        return new ArrayList<Interval>();

    }

    @Override
    public List<Interval> getIntervalsForTemplate(IntervalTemplate template) throws SQLException {
        List<IntervalForTemplateJoin> list = getIntervalForTemplateRuntimeDao().queryForAll();
        List<Interval> intervals = new ArrayList<Interval>();
        for(IntervalForTemplateJoin join : list){
            if(join.getTemplate().getId() == template.getId()){
                intervals.add(join.getInterval());
            }
        }
        return intervals;
    }

    @Override
    public List<Interval> getAllInterval() {
        return getIntervalRuntimeDao().queryForAll();
    }

    @Override
    public Interval getIntervalById(int id) {
        return getIntervalRuntimeDao().queryForId(id);
    }

    @Override
    public Statistic getStatisticForTraining(Training training){
        return new Statistic();
    }

    @Override
    public Statistic getStatisticForInterval(Interval interval){
        return new Statistic();
    }

    @Override
    public List<IntervalTemplate> getAllIntervalTemplate() {
        return getIntervalTemplatesRuntimeDao().queryForAll();
    }

    @Override
    public IntervalTemplate getIntervalTemplateById(int id) {
        return getIntervalTemplatesRuntimeDao().queryForId(id);
    }

    @Override
    public List<NodesTemplate> getAllNodesTemplate() {
        return getNodeTemplateRuntimeDao().queryForAll();
    }

    @Override
    public NodesTemplate getNodesTemplateById(int id) {
        return getNodeTemplateRuntimeDao().queryForId(id);
    }

    @Override
    public List<TrainingNode> getNodesForTemplate(NodesTemplate template) throws SQLException {
        return lookupTrainingNodesForTemplate(template);
    }

    @Override
    public void addTraining(Training training) {
        getTrainingRuntimeDao().create(training);
    }

    @Override
    public void removeTraining(Training training) {
        getTrainingRuntimeDao().delete(training);
    }

    @Override
    public void editTraining(Training training) {
        getTrainingRuntimeDao().update(training);
    }

    @Override
    public void addStatistic(Statistic statistic) {
        getStatisticRuntimeDao().create(statistic);
    }

    @Override
    public void editStatistic(Statistic statistic) {
        getStatisticRuntimeDao().update(statistic);
    }

    @Override
    public void removeStatistic(Statistic statistic) {
        getStatisticRuntimeDao().delete(statistic);
    }

    @Override
    public void addGpsSample(GpsSample sample) {
        getGpsSampleRuntimeDao().create(sample);
    }

    @Override
    public void removeGpsSample(GpsSample sample) {
        getGpsSampleRuntimeDao().delete(sample);
    }

    @Override
    public void addListGpsSample(List<GpsSample> samples) {
        for(GpsSample sample: samples){
            getGpsSampleRuntimeDao().create(sample);
        }
    }

    @Override
    public void addIntervalTemplate(IntervalTemplate template) {
        getIntervalTemplatesRuntimeDao().create(template);
    }

    @Override
    public void removeIntervalTemplate(IntervalTemplate template) {
        List<IntervalForTemplateJoin> list = getIntervalForTemplateRuntimeDao().queryForAll();
        for(IntervalForTemplateJoin join : list){
            if(join.getId() == template.getId()) {
                getIntervalForTemplateRuntimeDao().delete(join);
            }
        }
        getIntervalTemplatesRuntimeDao().delete(template);
    }

    @Override
    public void addIntervalToTemplate(IntervalTemplate template, Interval interval) {
        IntervalForTemplateJoin join = new IntervalForTemplateJoin(template, interval);
        getIntervalForTemplateRuntimeDao().create(join);
    }

    @Override
    public void addIntervalToTemplate(IntervalForTemplateJoin templateJoin) {
        getIntervalForTemplateRuntimeDao().create(templateJoin);
    }

    @Override
    public void addInterval(Interval interval) {
        getIntervalRuntimeDao().create(interval);
    }

    @Override
    public void removeInterval(Interval interval) {
        getIntervalRuntimeDao().delete(interval);
    }

    @Override
    public void addNodesTemplate(NodesTemplate template) {
        getNodeTemplateRuntimeDao().create(template);
    }

    @Override
    public void removeNodesTemplate(NodesTemplate template) {
        // TODO: Tu występuje NullPointerExceptions, trzeba sprawdzić gdzie i go naprawić
        List<NodeForTemplateJoin> joins = getNodeTemplateJoinRuntimeDao().queryForAll();
        for(NodeForTemplateJoin join : joins){
            if(join.getTemplate().getId() == template.getId()){
                getNodeTemplateJoinRuntimeDao().delete(join);
            }
        }

        List<NodeForTemplateJoin> afterDeleteList = getNodeTemplateJoinRuntimeDao().queryForAll();
        if(afterDeleteList.size() == 0){
            try {
                TableUtils.clearTable(getConnectionSource(), TrainingNode.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        getNodeTemplateRuntimeDao().delete(template);
    }

    @Override
    public void addNodeToTemplate(NodesTemplate template, TrainingNode node) {
        getNodeTemplateJoinRuntimeDao().create(new NodeForTemplateJoin(template, node));
    }

    @Override
    public void addNodeToTemplate(NodeForTemplateJoin templateJoin) {
        getNodeTemplateJoinRuntimeDao().create(templateJoin);
    }

    @Override
    public void addNode(TrainingNode node) {
        getNodeRuntimeDao().create(node);
    }

    @Override
    public void removeNode(TrainingNode node) {
        getNodeRuntimeDao().delete(node);
    }
}
