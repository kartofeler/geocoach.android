package pl.szmajnta.geocoach.app.Tracking.Activities.TrainingCreation;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import pl.szmajnta.geocoach.app.CustomControls.GpsStatusView;
import pl.szmajnta.geocoach.app.CustomControls.SelectTrainingView;
import pl.szmajnta.geocoach.app.IlluminationBar.IlluminationIntent;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsDeviceManager;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsStatusListener;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingType;
import pl.szmajnta.geocoach.app.Model.Entities.Types;
import pl.szmajnta.geocoach.app.Model.RouteSettings;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.Tracking.Activities.MainTrackerActivity;
import pl.szmajnta.geocoach.app.VibrateUtils;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class RouteSettingsActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> implements GpsStatusListener {

    private static final int NODE_TEMPLATE_SELECT_CODE = 1001;
    private static final int INTERV_TEMPLATE_SELECT_CODE = 1002;
    private Types trainingType = Types.NORMAL;
    boolean isXperia = true;
    private GpsDeviceManager gpsManager;
    private RouteSettings settings;

    private GpsStatusView gpsView;
    private SelectTrainingView selectView;

    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_route_setting);

        Intent checkIntent = new Intent(IlluminationIntent.ACTION_STOP_LED);

//        gpsManager = new GpsDeviceManager(this);
//        gpsManager.addGpsStatusObserver(this);

        gpsManager.searchForSatellites();

        if (null == getPackageManager().resolveService(checkIntent,
                PackageManager.GET_RESOLVED_FILTER)) {
            isXperia = false;
        }
        TrainingType selectedType = null;

        ImageButton nodeButton = (ImageButton)findViewById(R.id.imNode);
        nodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( RouteSettingsActivity.this, NodeTemplateChooseActivity.class);
                startActivityForResult(intent, NODE_TEMPLATE_SELECT_CODE );
                VibrateUtils.VibrateShort(RouteSettingsActivity.this);
            }
        });

        ImageButton intervButton = (ImageButton)findViewById(R.id.imInterv);
        intervButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(RouteSettingsActivity.this);
            }
        });
        gpsView = (GpsStatusView)findViewById(R.id.gps_status_view);

        gpsView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                boolean isEnabled = gpsView.toggleEnabled();
                if(isEnabled) gpsManager.startGps();
                else gpsManager.stopGps();
                RouteSettings.getInstance().setGpsOn(isEnabled);
                return false;
            }
        });

        selectView = (SelectTrainingView)findViewById(R.id.select_view);

        EditText nameEdit = (EditText)findViewById(R.id.etRouteName);
        nameEdit.setText("Trasa " + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == NODE_TEMPLATE_SELECT_CODE){
            if(resultCode == RESULT_OK) {
                int templateId = data.getIntExtra("result", -1);
                if (templateId != -1) {
                    try {
                        selectView.setNodeTemplate(templateId);
                        RouteSettings.getInstance().setSelectedNodeTemplate(templateId);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void OnSatellitesCountChanged(int count) {
        if(gpsView != null) {
            gpsView.setSattelitesCount(count);
        }
    }

    @Override
    public void OnFirstFix() {

    }

    @Override
    public void OnAccuracyChanged(float accuracy) {

        gpsView.setAccuracy(accuracy);
    }

    @Override
    public void OnLocationChanged(Location location) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        gpsManager.searchForSatellites();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        gpsManager.removeGpsStatusObserver(this);
        gpsManager.stopGps();
    }

    public void buttonNextClick(View view) {
        EditText nameEdit = (EditText) findViewById(R.id.etRouteName);
        VibrateUtils.VibrateShort(this);
        RouteSettings.getInstance().setRouteName(nameEdit.getText().toString());

        // Jeśli rodzaj treningu jest inny od zwykłego to bieganie bez GPSa jest bez sensu
        if (!gpsView.isGood() && selectView.getTrainingType() != 0) {
            selectView.setNoteDownScreen("Ten rodzaj treningu wymaga sygnału GPS");
        } else {
            if (selectView.isTrainingComplete()) {
                Intent intent = new Intent(this, MainTrackerActivity.class);
                startActivity(intent);
            }
        }
    }
}