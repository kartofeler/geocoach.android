package pl.szmajnta.geocoach.app.Interfaces;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-06-08.
 */
public interface TrainingProgressKeeper {

    public List<Double> getPaceHistory();
    public List<Double> getDistanceHistory();
    public List<Double> getAltitudeHistory();
    public List<Double> getKmsTimes();
    public void getBestTimeForOneKm(TrainingProgressCallback callback);
    public void getBestTimeForTwoKm(TrainingProgressCallback callback);
    public void getBestTimeForFiveKm(TrainingProgressCallback callback);
    public void getBestTimeForTenKm(TrainingProgressCallback callback);
    public void getBestTimeForTwentyKm(TrainingProgressCallback callback);
    public void getBestTimeForMaraton(TrainingProgressCallback callback);

    public void getBestDistanceForFiveMin(TrainingProgressCallback callback);
    public void getBestDistanceForCouper(TrainingProgressCallback callback);
    public void getBestDistanceForHalfHour(TrainingProgressCallback callback);
    public void getBestDistanceForHour(TrainingProgressCallback callback);
}
