package pl.szmajnta.geocoach.app.CustomControls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.NodesTemplate;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingType;
import pl.szmajnta.geocoach.app.Model.RouteSettings;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.VibrateUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-07-30.
 */
public class SelectTrainingView extends LinearLayout {

    private List<TrainingType> types;
    private int trainingType = 0;

    public SelectTrainingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.select_training_view, this, true);

        types = new TrackerDatabaseHelper(getContext()).getAllTrainingType();

        Button nextTypeButton = (Button)findViewById(R.id.buttonNextType);
        nextTypeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                trainingType++;
                if(trainingType > 3) trainingType = 0;
                prepareTypeOnLay(trainingType);
                VibrateUtils.VibrateShort(getContext());
            }
        });
    }

    public int getTrainingType() {
        return trainingType;
    }

    public boolean isTrainingComplete(){
        if(trainingType == 0){
            return true;
        } else if(trainingType == 1){
            boolean nodeComplete =  RouteSettings.getInstance().getSelectedNodeTemplate() != -1 ? true : false;
            if(!nodeComplete) setNoteDownScreen("Nie wybrano szablonu treningu na orientację");
            return nodeComplete;
        } else if(trainingType == 2){
            return true;
        } else if(trainingType == 3){
            boolean nodeComplete = RouteSettings.getInstance().getSelectedNodeTemplate() != -1 ? true : false;
            boolean intervComplete = true;
            boolean result = (nodeComplete && intervComplete);
            if(!result) setNoteDownScreen("Nie wybrano szablonów dla tego typu treningu");
            return result;
        }
        return false;
    }

    public void setNodeTemplate(int id) throws SQLException {
        TrackerDatabaseHelper helper = new TrackerDatabaseHelper(getContext());
        NodesTemplate template = helper.getNodesTemplateById(id);
        List<TrainingNode> nodes = helper.getNodesForTemplate(template);

        TextView views1 = (TextView)findViewById(R.id.firstLineNode);
        TextView views3 = (TextView)findViewById(R.id.tvNumberNode);

        views1.setText(template.getName());
        views3.setText(String.valueOf(nodes.size()));
    }

    public void setNoteDownScreen(String message){
        TextView view = (TextView)findViewById(R.id.textViewNote);
        view.setText(message);
    }

    private void prepareTypeOnLay(int type) {
        Button nextTypeButton = (Button)findViewById(R.id.buttonNextType);
        LinearLayout nodeLinear = (LinearLayout)findViewById(R.id.nodePartLinear);
        LinearLayout intervalLinear = (LinearLayout)findViewById(R.id.intervPartLinear);

        RouteSettings.getInstance().setType(types.get(type));
        clearSelection();

        if(type == 1){
            nextTypeButton.setText("Trening z orientacją");
            nodeLinear.setVisibility(VISIBLE);
            intervalLinear.setVisibility(INVISIBLE);
        } else if(type == 2){
            nextTypeButton.setText("Trening interwałowy");
            nodeLinear.setVisibility(INVISIBLE);
            intervalLinear.setVisibility(VISIBLE);
        } else if(type == 3){
            nextTypeButton.setText("Trening interwałowy z orientacją");
            nodeLinear.setVisibility(VISIBLE);
            intervalLinear.setVisibility(VISIBLE);
        } else{
            nextTypeButton.setText("Trening zwykły");
            nodeLinear.setVisibility(INVISIBLE);
            intervalLinear.setVisibility(INVISIBLE);
        }
    }

    private void clearSelection(){
        TextView[] views = new TextView[5];

        views[0] = (TextView)findViewById(R.id.firstLineInterv);
        views[1] = (TextView)findViewById(R.id.tvNumberInterv);

        views[2] = (TextView)findViewById(R.id.firstLineNode);
        views[3] = (TextView)findViewById(R.id.tvNumberNode);
        views[4] = (TextView)findViewById(R.id.textViewNote);

        for(int i = 0; i <5; i++){
            views[i].setText("");
        }

        views[0].setText("Wybierz szablon treningu interwałowego");
        views[2].setText("Wybierz szablon treningu na orientację");

        RouteSettings.getInstance().setSelectedNodeTemplate(-1);
        RouteSettings.getInstance().setType(types.get(0));
    }
}
