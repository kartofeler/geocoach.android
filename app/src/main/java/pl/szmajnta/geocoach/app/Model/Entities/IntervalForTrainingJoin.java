package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class IntervalForTrainingJoin {

    public final static String COLUMN_TRAINING_ID = "training_id";
    public final static String COLUMN_INTERVAL_ID = "interval_id";
    public final static String COLUMN_ID = "id";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private int id;
    @DatabaseField(foreign = true, columnName = COLUMN_TRAINING_ID, foreignAutoRefresh = true)
    private Training training;
    @DatabaseField(foreign = true, columnName = COLUMN_INTERVAL_ID, foreignAutoRefresh = true)
    private Interval interval;

    public IntervalForTrainingJoin() {
    }

    public IntervalForTrainingJoin(Training training, Interval interval) {
        this.training = training;
        this.interval = interval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    public Interval getInterval() {
        return interval;
    }

    public void setInterval(Interval interval) {
        this.interval = interval;
    }
}
