package pl.szmajnta.geocoach.app.restapi;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.RequiresHeader;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import pl.szmajnta.geocoach.app.restapi.model.ApiUser;
import pl.szmajnta.geocoach.app.restapi.model.request.RegisterData;
import pl.szmajnta.geocoach.app.restapi.model.response.RegisterResponse;
import pl.szmajnta.geocoach.app.restapi.model.response.UserInfoResponse;

/**
 * Created by Andrzej on 2015-04-18.
 */
@Rest(rootUrl = "http://api.geocoach.pl/app_dev.php/api",
        converters = { MappingJackson2HttpMessageConverter.class },
        interceptors = { HttpErrorLogging.class })
public interface GeocoachAPI extends RestClientErrorHandling {

    @Get("/user")
    @RequiresHeader("Authorization")
    public UserInfoResponse getUserInformationAction();

    @Get("/user/friends")
    @RequiresHeader("Authorization")
    public ApiUser getUserFriends();

    void setHeader(String name, String value);
    String getHeader(String name);
}
