package pl.szmajnta.geocoach.app.Tracking.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import pl.szmajnta.geocoach.app.Interfaces.TrainingProgressKeeper;
import pl.szmajnta.geocoach.app.R;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-03.
 */
public class ChartsFragment extends BaseTrackerFragment {

//    private GraphicalView mChart;
    private String chartState = "pace";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.frag_charts, container, false);

        if(savedInstanceState != null){
            chartState = savedInstanceState.getString("chartState");
        } else{
            chartState = "pace";
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshScreen(view);
            }
        });

        Button butPace = (Button)view.findViewById(R.id.buttonPace);
        butPace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartState = "pace";
                refreshScreen(view);
            }
        });

        Button butAltitude = (Button)view.findViewById(R.id.buttonAltitude);
        butAltitude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartState = "altitude";
                refreshScreen(view);
            }
        });

        refreshScreen(view);


        return view;
    }

    private void refreshScreen(View view) {

        TrainingProgressKeeper archive = getTrackerActivity().getArchive();
        if(archive != null) {
            LinearLayout chartContainer = (LinearLayout) view.findViewById(R.id.chart);
            chartContainer.removeAllViews();

            List<Double> pace = archive.getPaceHistory();
            List<Double> altitude = archive.getAltitudeHistory();
            List<Double> distance = archive.getDistanceHistory();

//            XYSeries xSeries = new XYSeries("");
//            XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
//            XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
//            XYSeriesRenderer xySeriesRenderer = new XYSeriesRenderer();
//
//
//            mRenderer.setXAxisMin(0);
//            mRenderer.setYAxisMin(0);
//            mRenderer.setShowGrid(true);
//            if(chartState.equalsIgnoreCase("pace")) {
//                for (int i = 0; i < distance.size(); i++) {
//                    xSeries.add(distance.get(i)/1000, pace.get(i));
//                }
//                xSeries.setTitle("Tempo");
//                mRenderer.setChartTitle("Wykres tempa");
//                mRenderer.setXTitle("Dystans [km]");
//                mRenderer.setYTitle("Tempo [min/km]");
//
//            }else if(chartState.equalsIgnoreCase("altitude")){
//                for (int i = 0; i < distance.size(); i++) {
//                    xSeries.add(distance.get(i)/1000, altitude.get(i));
//                }
//                xSeries.setTitle("Wysokość");
//                mRenderer.setChartTitle("Wykres wysokości");
//                mRenderer.setXTitle("Dystans [km]");
//                mRenderer.setYTitle("Wysokość [m]");
//            }
//            mRenderer.setYAxisMax(xSeries.getMaxY());
//            mRenderer.setXAxisMax(xSeries.getMaxX());
//
//            dataset.addSeries(xSeries);
//            xySeriesRenderer.setColor(Color.GREEN);
//            xySeriesRenderer.setPointStyle(PointStyle.CIRCLE);
//            xySeriesRenderer.setLineWidth(2);
//            xySeriesRenderer.setFillPoints(true);
//
//            mRenderer.addSeriesRenderer(xySeriesRenderer);
//            mRenderer.setBackgroundColor(getResources().getColor(R.color.background_dark_blue));
//
//            mChart = ChartFactory.getLineChartView(getTrackerActivity(), dataset, mRenderer);
//            chartContainer.addView(mChart);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("chartState", chartState);
    }
}