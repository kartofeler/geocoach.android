package pl.szmajnta.geocoach.app.Tracking.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.CommonHelpers.FileHelper;
import pl.szmajnta.geocoach.app.CommonHelpers.PopUpHelper;
import pl.szmajnta.geocoach.app.Export.KmlHelper;
import pl.szmajnta.geocoach.app.Export.Placemark;
import pl.szmajnta.geocoach.app.Export.TrainingRoute;
import pl.szmajnta.geocoach.app.Menu.SplashActivity;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.GpsSample;
import pl.szmajnta.geocoach.app.Model.Entities.Interval;
import pl.szmajnta.geocoach.app.Model.Entities.Statistic;
import pl.szmajnta.geocoach.app.Model.Entities.Training;
import pl.szmajnta.geocoach.app.R;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by The Ja on 2014-06-05.
 */
public class SummaryActivity extends Activity {

    private final int TO_KML_FILE = 0, TO_FACEBOOK = 1;

    private TrackerDatabaseHelper dbHelper;
    private Training training;

    private List<Interval> intervals;

    private List<GpsSample> samples;
    private TrainingRoute route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_summary);

        int trainingId = getIntent().getIntExtra("trainingId", -1);
        if(trainingId != -1) {
            dbHelper = new TrackerDatabaseHelper(this);
            training = dbHelper.getTrainingById(trainingId);
            // Fill the info in the view
            Statistic stat = training.getStatistic();
            // Time
            TextView textViewToChange = (TextView) findViewById(R.id.tvSummaryTime);
            textViewToChange.setText(Integer.toString(stat.getTotalTime()));
            // Distance
            textViewToChange = (TextView) findViewById(R.id.tvSummaryDistance);
            textViewToChange.setText(Float.toString(stat.getTotalDistance()));
            // Avg Speed
            textViewToChange = (TextView) findViewById(R.id.tvSummaryAvgSpeed);
            textViewToChange.setText(Float.toString(stat.getAvgSpeed()));
            // Max Speed
            textViewToChange = (TextView) findViewById(R.id.tvSummaryMaxSpeed);
            textViewToChange.setText(Float.toString(stat.getMaxSpeed()));
            // Max Pace
            textViewToChange = (TextView) findViewById(R.id.tvSummaryMaxPace);
            textViewToChange.setText(Float.toString(stat.getMaxPace()));
            // Avg Pace
            textViewToChange = (TextView) findViewById(R.id.tvSummaryAvgPace);
            textViewToChange.setText(Float.toString(stat.getAvgPace()));
            // Min Altitude
            textViewToChange = (TextView) findViewById(R.id.tvSummaryMinAltitude);
            textViewToChange.setText(Float.toString(stat.getMinAltitude()));
            // Max Altitude
            textViewToChange = (TextView) findViewById(R.id.tvSummaryMaxAltitude);
            textViewToChange.setText(Float.toString(stat.getMaxAltitude()));
            // Max Slop
            textViewToChange = (TextView) findViewById(R.id.tvSummaryMaxSlope);
            textViewToChange.setText(Float.toString(stat.getMaxSlope()));
        }
    }

    @Override
    public void onBackPressed(){
        if(getIntent().getStringExtra("prevActivity").equalsIgnoreCase("tracking")) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
        } else{
            onBackPressed();
        }
    }

    public void buttonDetailsClick(View view){
        // Get intervals from training
        // TODO: Get the intervals from DB and delete this:
        intervals = new ArrayList<Interval>(3);
        intervals.add(new Interval("Pierwszy", null, null, 0.0f));
        intervals.add(new Interval("Drugi", null, null, 0.0f));
        intervals.add(new Interval("Trzeci", null, null, 0.0f));
        //intervals = dbHelper.getIntervalsForTraining(training);

        // Display the dialog
        if(intervals.size() > 0) {
            // Create array of names
            CharSequence[] intervalsNames = new CharSequence[intervals.size()];
            for (int i = 0; i < intervals.size(); i++)
                intervalsNames[i] = intervals.get(i).getName();

            new AlertDialog.Builder(this)
                    .setTitle("Wybierz interwał:")
                    .setItems(intervalsNames, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            executeIntervalDetails(which);
                        }
                    }).show();
        }
        // Display error message
        else
            PopUpHelper.displayInfoPopUp(this, "Ten trening nie zawierał żadnych interwałów.", "Brak interwałów");
    }

    public void buttonExportClick(View view){
        if(dbHelper == null) return;
        samples = dbHelper.getAllSampleForTraining(training);

        // TEMP!
        samples.add(new GpsSample(18.64146f, 50.30113f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64117f, 50.30124f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64063f,50.30128f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64039f,50.30126f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64029f,50.30116f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64015f,50.30071f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.6402f,50.30068f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64006f,50.30048f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64734f,50.2974f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.6482f,50.29707f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.64996f,50.29649f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65089f,50.2961f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65446f,50.2984f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65902f,50.30135f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65935f,50.30147f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65907f,50.30204f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65898f,50.30265f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65905f,50.30316f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65924f,50.3036f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65949f,50.30401f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.65962f,50.30416f, 0.0f, 0.0f, null));
        samples.add(new GpsSample(18.66135f,50.3055f, 0.0f, 0.0f, null));

        training = new Training();
        training.setName("Nowa trasa");
        training.setDate(new Date(2014,6,8));
        // END TEMP

        // Create route object for export
        route = new TrainingRoute();
        route.setName(training.getName());

        // Include Placemarks
        ArrayList<Placemark> placemarks = new ArrayList<Placemark>();
        placemarks.add(new Placemark("Start", samples.get(0)));
        placemarks.add(new Placemark("Meta", samples.get(samples.size() - 1)));
        route.setPlacemarks(placemarks);

        // Include samples
        route.setSamples(samples);

        CharSequence[] options = new CharSequence[2];
        options[TO_KML_FILE] = "Eksportuj do KML";
        options[TO_FACEBOOK] = "facebook";

        new AlertDialog.Builder(this)
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SummaryActivity.this.executeExport(which);
                    }
                }).show();
    }

    private void executeIntervalDetails(int chosenInterval){

    }

    private void executeExport(int chosenExport) {
        switch (chosenExport){
            case TO_KML_FILE:
                try {
                    if (FileHelper.isExternalStorageWritable()) {
                        KmlHelper helper = new KmlHelper();
                        String fileName = training.getName();
                        fileName += "(" + training.getDate().toString() + ")";
                        fileName += ".kml";
                        helper.setRoute(route);
                        FileHelper.saveDataToPublicFile(fileName, helper.Export());
                        PopUpHelper.displayInfoPopUp(this, "Twója trasa została zapisana w pliku: " + fileName,
                                "Zakończono");
                    }
                    else
                        PopUpHelper.displayInfoPopUp(this, "Nie udało się utworzyć pliku!", "Błąd");
                }
                catch (Exception e){
                    PopUpHelper.displayInfoPopUp(this, "Nie udało się utworzyć pliku!", "Błąd");
                }
                break;
            case TO_FACEBOOK:
                PopUpHelper.displayInfoPopUp(this, "Tutaj będzie eksport do facebooka");
                break;
        }
    }
}
