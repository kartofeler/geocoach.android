package pl.szmajnta.geocoach.app.Interfaces;

/**
 * Created by Andrzej Laptop on 2014-05-23.
 */
public interface MagneticDeviceManager {
    public void addMagneticInfoListener(MagneticDeviceListener listener);
    public void removeMagneticInfoListener(MagneticDeviceListener listener);

    public void magneticOrientationChanged(float[] values, int accuracy);
    public void startMagneticDevice();
    public void stopMagneticDevice();

}
