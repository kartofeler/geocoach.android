package pl.szmajnta.geocoach.app.CustomControls;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;
import pl.szmajnta.geocoach.app.R;

/**
 * Created by Andrzej Laptop on 2014-05-25.
 */
public class NodeControl extends LinearLayout {
    public NodeControl(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_current_node, this, true);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.NodeCustom,
                0, 0);

        try {
//            Integer reference = a.getColor(R.styleable.NodeCustom_color, 0);
            TextView view = (TextView) findViewById(R.id.rectangle);
//            view.setBackgroundColor(reference);
//            view.setTextColor(reference);
        } finally {
            a.recycle();
        }
    }

    public void setNode(TrainingNode node){
        TextView tvLatitude = (TextView)findViewById(R.id.tvLatitudee);
        TextView tvLongitude = (TextView)findViewById(R.id.tvLongitude);

        tvLatitude.setText(String.valueOf(node.getLatitude()));
        tvLongitude.setText(String.valueOf(node.getLongitude()));
    }
}
