package pl.szmajnta.geocoach.app.Export;

import pl.szmajnta.geocoach.app.Export.Exceptions.ExportException;

/**
 * Created by The Ja on 2014-06-07.
 */
public interface RouteExporter {

    public void setRoute(TrainingRoute route);
    public TrainingRoute getRoute();

    public String Export()  throws ExportException;
}
