package pl.szmajnta.geocoach.app.Model;

import android.location.Location;
import pl.szmajnta.geocoach.app.Interfaces.NodeManagerInterface;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-24.
 */
public class BaseNodeManager implements NodeManagerInterface {

    private static int NODE_REACHED_RADIUS = 30;
    private ArrayList<TrainingNode> nodes = new ArrayList<TrainingNode>();
    private int currentNodeIndex = 0;

    public BaseNodeManager(ArrayList<TrainingNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    public int getCurrentNodeIndex() {
        return currentNodeIndex;
    }

    @Override
    public int getNodesCount() {
        return nodes.size();
    }

    @Override
    public TrainingNode getCurrentNode() {
        return nodes.get(currentNodeIndex);
    }

    @Override
    public TrainingNode getNextNode() {
        if(currentNodeIndex + 1 >= nodes.size()) {
            return null;
        } else {
            return nodes.get(currentNodeIndex + 1);
        }
    }

    @Override
    public TrainingNode getPreviousNode() {
        if(currentNodeIndex - 1 < 0) {
            return null;
        } else {
            return nodes.get(currentNodeIndex - 1);
        }
    }

    @Override
    public TrainingNode switchToNextNode() {
        currentNodeIndex++;
        return nodes.get(currentNodeIndex);
    }

    @Override
    public boolean checkSwitchPossible(Location location) {
        boolean isSwitch = false;

        Location targetLocation = nodes.get(currentNodeIndex).getLocation();

        if(location.distanceTo(targetLocation) < NODE_REACHED_RADIUS){
            isSwitch = true;
        }

        return isSwitch;
    }

    @Override
    public boolean checkSwitchPossible(float latitude, float longitude) {
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        return checkSwitchPossible(location);
    }

    @Override
    public List<TrainingNode> getAllNodes() {
        return nodes;
    }
}
