package pl.szmajnta.geocoach.app.globals;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by andrzej on 18.05.15.
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface GpsPreferences {
    @DefaultInt(3) int gpsTime();
    @DefaultInt(20) int minAccuracy();
}
