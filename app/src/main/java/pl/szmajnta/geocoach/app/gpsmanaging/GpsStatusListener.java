package pl.szmajnta.geocoach.app.gpsmanaging;

import android.location.Location;

/**
 * Created by Andrzej Laptop on 2014-05-09.
 */
public interface GpsStatusListener {
    public void OnSatellitesCountChanged(int count);
    public void OnFirstFix();
    public void OnAccuracyChanged(float accuracy);
    public void OnLocationChanged(Location location);
}
