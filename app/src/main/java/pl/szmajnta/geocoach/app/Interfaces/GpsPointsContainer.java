package pl.szmajnta.geocoach.app.Interfaces;

import android.location.Location;
import pl.szmajnta.geocoach.app.Model.MeasureSample;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public interface GpsPointsContainer {
    public void addGpsPoint(Location location);
    public Location getLastLocation();
    public Location getGpsPoint(int index);
    public List<Location> getAllPoints();
    public void clearAll();

    public double getTotalDistance();
    public MeasureSample getMeasureSample();
}
