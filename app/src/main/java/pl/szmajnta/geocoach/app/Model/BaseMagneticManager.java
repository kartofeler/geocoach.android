package pl.szmajnta.geocoach.app.Model;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import pl.szmajnta.geocoach.app.Interfaces.MagneticDeviceListener;
import pl.szmajnta.geocoach.app.Interfaces.MagneticDeviceManager;

import java.util.ArrayList;

/**
 * Created by Andrzej Laptop on 2014-05-24.
 */
public class BaseMagneticManager implements MagneticDeviceManager, SensorEventListener {

    private ArrayList<MagneticDeviceListener> listeners = new ArrayList<MagneticDeviceListener>();
    private Context context;
    private SensorManager sensorManager;

    public BaseMagneticManager(Context context) {
        this.context = context;
    }

    @Override
    public void addMagneticInfoListener(MagneticDeviceListener listener) {
        if(!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeMagneticInfoListener(MagneticDeviceListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void magneticOrientationChanged(float[] values, int accuracy) {
        for(MagneticDeviceListener list : listeners){
            list.OnMagneticOrientationChanged(values, accuracy);
        }
    }

    @Override
    public void startMagneticDevice() {
        sensorManager = (SensorManager) context.getSystemService(Activity.SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void stopMagneticDevice() {
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        magneticOrientationChanged(sensorEvent.values, sensorEvent.accuracy);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
