package pl.szmajnta.geocoach.app.gpsmanaging;

import android.location.Location;

import pl.szmajnta.geocoach.app.base.BaseSubject;

/**
 * Created by Andrzej Laptop on 2014-05-09.
 */
public interface GpsStatusSubject extends BaseSubject<GpsStatusSubject> {

}
