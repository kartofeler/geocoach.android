package pl.szmajnta.geocoach.app.Tracking.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import pl.szmajnta.geocoach.app.Tracking.Fragments.*;

public class IntervalTrainingAdapter extends TrackerPagerAdapter {

    @Override
    public String[] getHeader() {
        return new String[]{"Mapa", "Wykresy", "Pomiary", "Etapy"};
    }

    public IntervalTrainingAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getItem(int index) {
        switch (index) {
            case 0:
                return new MapFragment();
            case 1:
                return new ChartsFragment();
            case 2:
                return new MeasureFragment();
            case 3:
                return new IntervalFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
