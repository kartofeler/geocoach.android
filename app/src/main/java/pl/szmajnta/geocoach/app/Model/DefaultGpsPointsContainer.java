package pl.szmajnta.geocoach.app.Model;

import android.location.Location;
import pl.szmajnta.geocoach.app.Interfaces.GpsPointsContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public class DefaultGpsPointsContainer implements GpsPointsContainer {
    private ArrayList<Location> locationList = new ArrayList<Location>();

    private MeasureSample measureSample = new MeasureSample();

    @Override
    public void addGpsPoint(Location location) {

        if (measureSample.gpsPoints == 0) {
            measureSample.momentVelocity = location.getSpeed();
            measureSample.maxVelocity = location.getSpeed();
            measureSample.meanVelocity = location.getSpeed();
            measureSample.altitude = location.getAltitude();
            measureSample.minAltitude = location.getAltitude();
            measureSample.maxAltitude = location.getAltitude();
        }
        if (measureSample.gpsPoints > 0) {
            Location lastLocation = locationList.get(measureSample.gpsPoints - 1);
            float distance = lastLocation.distanceTo(location);
            measureSample.totalDistance += distance;
            measureSample.momentVelocity = (lastLocation.getSpeed() + location.getSpeed()) / 2;
            if (measureSample.momentVelocity > measureSample.maxVelocity) {
                measureSample.maxVelocity = measureSample.momentVelocity;
            }
            measureSample.meanVelocity = (measureSample.meanVelocity * (measureSample.gpsPoints - 1) + location.getSpeed()) / measureSample.gpsPoints;
            measureSample.altitude = location.getAltitude();
            if(location.getAltitude() > measureSample.maxAltitude) measureSample.maxAltitude = location.getAltitude();
            if(location.getAltitude() < measureSample.minAltitude) measureSample.minAltitude = location.getAltitude();


            measureSample.slope = Math.asin((location.getAltitude() - lastLocation.getAltitude())/distance);
        }
        measureSample.gpsPoints++;

        locationList.add(location);
    }

    @Override
    public Location getLastLocation() throws ArrayIndexOutOfBoundsException{
        int i = locationList.size() - 1;
        if(i < 0) {
            return null;
        }else {
            return locationList.get(locationList.size() - 1);
        }
    }

    @Override
    public Location getGpsPoint(int index) {
        return locationList.get(index);
    }

    @Override
    public List<Location> getAllPoints() {
        return locationList;
    }

    @Override
    public void clearAll() {
        locationList.clear();
    }

    @Override
    public double getTotalDistance() {
        return measureSample.totalDistance;
    }

    @Override
    public MeasureSample getMeasureSample() {
        return measureSample;
    }
}
