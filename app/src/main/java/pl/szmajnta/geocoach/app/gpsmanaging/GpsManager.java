package pl.szmajnta.geocoach.app.gpsmanaging;

/**
 * Created by andrzej on 18.05.15.
 */
public interface GpsManager {
    public void startGps();
    public void stopGps();
    public void searchForSatellites();
}
