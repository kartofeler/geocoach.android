package pl.szmajnta.geocoach.app.Interfaces;

/**
 * Created by Andrzej on 2014-08-17.
 */
public interface TrainingProgressCallback {
    public void resultCalculationDone(double value);
}
