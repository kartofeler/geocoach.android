package pl.szmajnta.geocoach.app.Model;

import android.content.Context;
import pl.szmajnta.geocoach.app.Interfaces.*;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDataRepository;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.NodesTemplate;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;
import pl.szmajnta.geocoach.app.gpsmanaging.GpsDeviceManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public class TrainingBuilder {

    private Context context;
    private GpsDeviceManager gpsManager;
    private GpsPointsContainer container;
    private RouteSettings routeSettings;
    private MagneticDeviceManager magneticManager;
    private NodeManagerInterface nodeManager;
    private IntervalManagerInterface intervalManager;

    public TrainingBuilder(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public GpsDeviceManager getGpsManager() {
        return gpsManager;
    }

    public GpsPointsContainer getContainer() {
        return container;
    }

   public RouteSettings getRouteSettings() {
        return routeSettings;
    }

    public MagneticDeviceManager getMagneticManager() {
        return magneticManager;
    }

    public NodeManagerInterface getNodeManager() {
        return nodeManager;
    }

    public IntervalManagerInterface getIntervalManager() {
        return intervalManager;
    }

    public TrainingBuilder setGpsManager(GpsDeviceManager gpsManager) {
        this.gpsManager = gpsManager;
        return this;
    }

    public void setRouteSettings(RouteSettings settings) {
        this.routeSettings = settings;
        setMagneticManager(new BaseMagneticManager(context));
        if(routeSettings.isGpsOn()){
            setGpsManager(new GpsDeviceManager());
            setGpsPointsContainer(new DefaultGpsPointsContainer());
        }
        if(routeSettings.getSelectedNodeTemplate() != -1){
            TrackerDataRepository helper = new TrackerDatabaseHelper(context);
            NodesTemplate template = helper.getNodesTemplateById(RouteSettings
                    .getInstance()
                    .getSelectedNodeTemplate());

            ArrayList<TrainingNode> nodes;

            try {
                nodes = (ArrayList)helper.getNodesForTemplate(template);
                setNodeManager(new BaseNodeManager(nodes));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private TrainingBuilder setContainer(GpsPointsContainer container) {
        this.container = container;
        return this;
    }

    private TrainingBuilder setGpsPointsContainer(GpsPointsContainer container) {
        this.container = container;
        return this;
    }

    private TrainingBuilder setContext(Context context) {
        this.context = context;
        return this;
    }

    private TrainingBuilder setMagneticManager(MagneticDeviceManager magneticManager) {
        this.magneticManager = magneticManager;
        return this;
    }

    private TrainingBuilder setNodeManager(NodeManagerInterface nodeManager) {
        this.nodeManager = nodeManager;
        return this;
    }

    private TrainingBuilder setIntervalManager(IntervalManagerInterface intervalManager) {
        this.intervalManager = intervalManager;
        return this;
    }

    public TrainingModel build() {
        return new BaseTraining(this);
    }
}
