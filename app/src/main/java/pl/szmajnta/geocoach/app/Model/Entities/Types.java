package pl.szmajnta.geocoach.app.Model.Entities;

/**
 * Created by Andrzej Laptop on 2014-06-04.
 */
public enum Types {
    NORMAL,
    ORIENTATION,
    INTERVAL,
    ORIENTATION_INTERVAL
}
