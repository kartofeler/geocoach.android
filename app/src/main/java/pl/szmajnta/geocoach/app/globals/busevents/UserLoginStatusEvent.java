package pl.szmajnta.geocoach.app.globals.busevents;

/**
 * Created by Andrzej on 2015-04-19.
 */
public class UserLoginStatusEvent {
    private boolean logged;

    public UserLoginStatusEvent(boolean logged) {
        this.logged = logged;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }
}
