package pl.szmajnta.geocoach.app.restapi.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import pl.szmajnta.geocoach.app.restapi.model.ApiUser;

/**
 * Created by Andrzej on 2015-04-19.
 */
public class UserInfoResponse extends ApiResponse {
    @JsonProperty("extras")
    private ApiUser extras;

    public UserInfoResponse(int code, String description, ApiUser extras) {
        super(code, description);
        this.extras = extras;
    }

    public UserInfoResponse() {
    }

    public ApiUser getExtras() {
        return extras;
    }

    public void setExtras(ApiUser extras) {
        this.extras = extras;
    }
}