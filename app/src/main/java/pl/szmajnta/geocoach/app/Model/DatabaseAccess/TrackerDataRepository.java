package pl.szmajnta.geocoach.app.Model.DatabaseAccess;

import pl.szmajnta.geocoach.app.Model.Entities.*;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-06-07.
 */
public interface TrackerDataRepository {
    public List<Training> getAllTraining();
    public Training getTrainingById(int id);

    public List<TrainingType> getAllTrainingType();

    public List<GpsSample> getAllSampleForTraining(Training training);

    public List<Interval> getIntervalsForTraining(Training training);
    public List<Interval> getIntervalsForTemplate(IntervalTemplate template) throws SQLException;
    public List<Interval> getAllInterval();
    public Interval getIntervalById(int id);

    public Statistic getStatisticForTraining(Training training);
    public Statistic getStatisticForInterval(Interval interval);

    public List<IntervalTemplate> getAllIntervalTemplate();
    public IntervalTemplate getIntervalTemplateById(int id);

    public List<NodesTemplate> getAllNodesTemplate();
    public NodesTemplate getNodesTemplateById(int id);

    public List<TrainingNode> getNodesForTemplate(NodesTemplate template) throws SQLException;

    public void addTraining(Training training);
    public void removeTraining(Training training);
    public void editTraining(Training training);

    public void addStatistic(Statistic statistic);
    public void editStatistic(Statistic statistic);
    public void removeStatistic(Statistic statistic);

    public void addGpsSample(GpsSample sample);
    public void removeGpsSample(GpsSample sample);
    public void addListGpsSample(List<GpsSample> samples);

    public void addIntervalTemplate(IntervalTemplate template);
    public void removeIntervalTemplate(IntervalTemplate template);

    public void addIntervalToTemplate(IntervalTemplate template, Interval interval);
    public void addIntervalToTemplate(IntervalForTemplateJoin templateJoin);

    public void addInterval(Interval interval);
    public void removeInterval(Interval interval);

    public void addNodesTemplate(NodesTemplate template);
    public void removeNodesTemplate(NodesTemplate template);

    public void addNodeToTemplate(NodesTemplate template, TrainingNode node);
    public void addNodeToTemplate(NodeForTemplateJoin templateJoin);

    public void addNode(TrainingNode node);
    public void removeNode(TrainingNode node);


}
