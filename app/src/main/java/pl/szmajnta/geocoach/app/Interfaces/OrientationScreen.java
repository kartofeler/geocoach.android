package pl.szmajnta.geocoach.app.Interfaces;

import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;

/**
 * Created by Andrzej Laptop on 2014-05-24.
 */
public interface OrientationScreen {
    public void newNorth( float zAxis);
    public void newOrientation(float zAxis, float distance);
    public void updateNodeInformation(int curr, int all, TrainingNode[] nodes);
}
