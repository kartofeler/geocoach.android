package pl.szmajnta.geocoach.app.CommonHelpers;

import java.util.*;

/**
 * Created by Andrzej Laptop on 2014-07-23.
 */
public class ListHelper {
    public static <T> List<T> iterableReverseList(final List<T> l) {
        Collections.reverse(l);
        return l;
    }
}
