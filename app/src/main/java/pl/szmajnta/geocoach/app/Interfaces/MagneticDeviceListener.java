package pl.szmajnta.geocoach.app.Interfaces;

/**
 * Created by Andrzej Laptop on 2014-05-24.
 */
public interface MagneticDeviceListener {
    public void OnMagneticOrientationChanged(float[] values, int accuracy);
}
