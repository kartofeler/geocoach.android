package pl.szmajnta.geocoach.app.CommonHelpers;

import android.os.Environment;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by The Ja on 2014-06-08.
 */
public class FileHelper {
    private static final String DEFAULT_FOLDER_NAME = "Tracker";

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static boolean saveDataToPublicFile(String fileName, String fileContent){
        return saveDataToPublicFile(fileName, fileContent, DEFAULT_FOLDER_NAME);
    }

    public static boolean saveDataToPublicFile(String fileName, String fileContent, String folderName){
        boolean result = false;

        // TODO: Change directory name
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), fileName);

        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(file));
            out.write(fileContent.getBytes());
            result = true;
        }
        catch (Exception e){}
        finally{
            if (out != null) {
                try{out.close();}
                catch(Exception e){}
            }
        }

        return result;
    }
}
