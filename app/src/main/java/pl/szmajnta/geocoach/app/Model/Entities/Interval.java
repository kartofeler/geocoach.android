package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class Interval {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String description;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private IntervalType type;
    @DatabaseField
    private float value;

    public Interval() {
    }

    public Interval(String name, String description, IntervalType type, float value) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IntervalType getType() {
        return type;
    }

    public void setType(IntervalType type) {
        this.type = type;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
