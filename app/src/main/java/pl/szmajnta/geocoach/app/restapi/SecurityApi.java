package pl.szmajnta.geocoach.app.restapi;

import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import pl.szmajnta.geocoach.app.restapi.model.request.LoginData;
import pl.szmajnta.geocoach.app.restapi.model.response.LoginResponse;
import pl.szmajnta.geocoach.app.restapi.model.request.RegisterData;
import pl.szmajnta.geocoach.app.restapi.model.response.RegisterResponse;

/**
 * Created by Andrzej on 2015-04-13.
 */
@Rest(rootUrl = "http://api.geocoach.pl/app_dev.php/security",
        converters = { MappingJackson2HttpMessageConverter.class },
        interceptors = { HttpErrorLogging.class })
public interface SecurityApi  extends RestClientErrorHandling {
    @Post("/register")
    public RegisterResponse registerAction(RegisterData data);
    @Post("/login")
    public LoginResponse loginAction(LoginData data);
}
