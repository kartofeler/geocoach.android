package pl.szmajnta.geocoach.app.CustomControls;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.R;

import java.text.DecimalFormat;

/**
 * Created by Andrzej Laptop on 2014-07-26.
 */
public class GpsStatusView extends LinearLayout {

    private boolean isEnabled = true;
    private boolean isGood = false;

    public GpsStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.gps_view, this, true);
    }

    public void setSattelitesCount(int satelliteCount){
        TextView textViewSatellites = (TextView)findViewById(R.id.sat_count_text);
        textViewSatellites.setText(String.valueOf(satelliteCount));
    }

    public void setAccuracy(float accuracy){
        TextView accuracyText = (TextView) findViewById(R.id.prox_text);
        ImageView signalStrengthView = (ImageView)findViewById(R.id.gps_status);
        Drawable gpsSignalDraw = null;
        if(accuracy <= 100) {
            isGood = true;
            String decimalAccuracy = new DecimalFormat("#.#").format(accuracy);
            accuracyText.setText(String.valueOf(decimalAccuracy) + "m");
            if(accuracy < 15){
                gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_3);
            } else if(accuracy < 35){
                gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_2);
            } else{
                gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_1);
            }

        } else{
            isGood = false;
            accuracyText.setText("Oczekiwanie...");
            gpsSignalDraw = getResources().getDrawable(R.drawable.gps_status_weak);
        }
        if(gpsSignalDraw != null){
            signalStrengthView.setImageDrawable(gpsSignalDraw);
        }
    }

    public boolean isGood() {
        return isGood;
    }

    public boolean toggleEnabled(){
        float alpha;
        if(isEnabled){
            alpha = 0.1f;
            isEnabled = false;
        } else{
            alpha = 1.0f;
            isEnabled = true;
        }

        setAlpha(alpha);
        return isEnabled;
    }
}
