package pl.szmajnta.geocoach.app.Model.Entities;

import android.location.Location;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */
public class GpsSample {

    public final static String COLUMN_ID = "id";
    public final static String COLUMN_LAT = "latitude";
    public final static String COLUMN_LONG = "longitude";
    public final static String COLUMN_ALTITUDE = "altitude";
    public final static String COLUMN_SPEED = "speed";
    public final static String COLUMN_TRAINING_ID = "training";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private int id;
    @DatabaseField(columnName = COLUMN_LAT)
    private float latitude;
    @DatabaseField(columnName = COLUMN_LONG)
    private float longitude;
    @DatabaseField(columnName = COLUMN_ALTITUDE)
    private float altitude;
    @DatabaseField(columnName = COLUMN_SPEED)
    private float speed;
    @DatabaseField(columnName = COLUMN_TRAINING_ID, foreign = true, foreignAutoRefresh = true)
    private Training training;


    public GpsSample() {
    }

    public GpsSample(float latitude, float longitude, float altitude, float speed, Training training) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.speed = speed;
        this.training = training;
    }

    public GpsSample(Location location, Training training){
        latitude = (float)location.getLatitude();
        longitude = (float)location.getLongitude();
        altitude = (float)location.getAltitude();
        speed = location.getSpeed();

        this.training = training;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }
}
