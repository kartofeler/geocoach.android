package pl.szmajnta.geocoach.app.Menu;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import pl.szmajnta.geocoach.app.R;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class ConfigureActivity extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.app_preference);
    }
}