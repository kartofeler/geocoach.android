package pl.szmajnta.geocoach.app.restapi.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrzej on 2015-04-13.
 */
public class LoginData {
    @JsonProperty("email")
    protected String email;
    @JsonProperty("password")
    protected String password;

    public LoginData(String email, String password) {
        this.email = email;
        this.password = password;
    }
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }
    @JsonProperty("password")
    public String getPassword() {
        return password;
    }
    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }
}
