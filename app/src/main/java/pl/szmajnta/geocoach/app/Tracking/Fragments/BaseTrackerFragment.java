package pl.szmajnta.geocoach.app.Tracking.Fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import pl.szmajnta.geocoach.app.Tracking.Activities.MainTrackerActivity;

/**
 * Created by Andrzej Laptop on 2014-05-03.
 */
public class BaseTrackerFragment extends Fragment {

    protected MainTrackerActivity getTrackerActivity(){
        FragmentActivity act = super.getActivity();
        return (MainTrackerActivity)act;
    }

}