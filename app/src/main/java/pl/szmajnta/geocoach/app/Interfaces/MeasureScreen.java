package pl.szmajnta.geocoach.app.Interfaces;

import pl.szmajnta.geocoach.app.Model.MeasureSample;

/**
 * Created by Andrzej Laptop on 2014-05-12.
 */
public interface MeasureScreen {
    public void newLocation(int time, MeasureSample sample);
    public void newTime(int seconds);
}
