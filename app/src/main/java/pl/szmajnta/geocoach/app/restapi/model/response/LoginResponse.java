package pl.szmajnta.geocoach.app.restapi.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import pl.szmajnta.geocoach.app.restapi.model.AccessToken;

/**
 * Created by Andrzej on 2015-04-13.
 */
public class LoginResponse extends ApiResponse{
    @JsonProperty("extras")
    private AccessToken extras;

    public LoginResponse() {
    }

    public LoginResponse(int code, String description, AccessToken extras) {
        super(code, description);
        this.extras = extras;
    }

    public LoginResponse(int code, String description) {
        super(code, description);
    }

    @JsonProperty("extras")
    public AccessToken getExtras() {
        return extras;
    }
    @JsonProperty("extras")
    public void setExtras(AccessToken extras) {
        this.extras = extras;
    }
}
