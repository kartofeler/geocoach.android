package pl.szmajnta.geocoach.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.res.IntegerRes;
import pl.szmajnta.geocoach.app.R;

@EFragment(R.layout.fragment_splash)
public class SplashFragment extends android.support.v4.app.Fragment {

    @IntegerRes(R.integer.time_splash)
    Integer splashTime;

    private FragmentInteractionListener listener;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    public SplashFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        listener.onSplashTimer();
                    }
                }, splashTime);
            }
        }, splashTime);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (FragmentInteractionListener)activity;
    }

    public interface FragmentInteractionListener {
        void onSplashTimer();
    }
}
