package pl.szmajnta.geocoach.app.Model;

import pl.szmajnta.geocoach.app.Model.Entities.TrainingType;

/**
 * Created by Andrzej Laptop on 2014-05-07.
 */
public class RouteSettings {

    private static RouteSettings instance;

    public static RouteSettings getInstance(){
        if(instance == null){
            instance = new RouteSettings();
        }
        return instance;
    }

    public RouteSettings() {
    }

    private String routeName;

    private String activityType = "Bieganie";

    private TrainingType type;

    private boolean gpsOn;

    private int selectedNodeTemplate = -1;

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public TrainingType getType() {
        return type;
    }

    public void setType(TrainingType type) {
        this.type = type;
    }

    public boolean isGpsOn() {
        // TODO !!! ZMIANA TYMCZASOWA!
        return true;
    }

    public void setGpsOn(boolean gpsOn) {
        this.gpsOn = gpsOn;
    }

    public int getSelectedNodeTemplate() {
        return selectedNodeTemplate;
    }

    public void setSelectedNodeTemplate(int selectedNodeTemplate) {
        this.selectedNodeTemplate = selectedNodeTemplate;
    }
}
