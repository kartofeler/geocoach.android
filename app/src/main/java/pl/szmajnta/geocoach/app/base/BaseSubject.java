package pl.szmajnta.geocoach.app.base;

/**
 * Created by andrzej on 18.05.15.
 */
public interface BaseSubject<T> {
    public void addListener(T listener);
    public void removeListener(T listener);
}
