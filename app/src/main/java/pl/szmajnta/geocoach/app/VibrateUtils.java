package pl.szmajnta.geocoach.app;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created by Andrzej Laptop on 2014-07-29.
 */
public class VibrateUtils {
    public final static int SHORT_VIBRATE_TIME_MS = 75;

    public static void VibrateShort(Context context){
        Vibrator vibe = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(SHORT_VIBRATE_TIME_MS);
    }
}
