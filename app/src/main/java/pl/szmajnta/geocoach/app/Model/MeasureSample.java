package pl.szmajnta.geocoach.app.Model;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public class MeasureSample {

    public double slope;
    public double totalDistance;
    public double meanVelocity;
    public double momentVelocity;
    public double maxVelocity;
    public double altitude;
    public double maxAltitude;
    public double minAltitude;
    public int gpsPoints;
}
