package pl.szmajnta.geocoach.app.Model.Entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Andrzej Laptop on 2014-06-06.
 */

public class IntervalForTemplateJoin {

    public final static String COLUMN_TEMPLATE_ID = "template_id";
    public final static String COLUMN_INTERVAL_ID = "interval_id";
    public final static String COLUMN_ID = "id";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID, allowGeneratedIdInsert = true)
    private int id;
    @DatabaseField(foreign = true, columnName = COLUMN_TEMPLATE_ID, foreignAutoRefresh = true)
    private IntervalTemplate template;
    @DatabaseField(foreign = true, columnName = COLUMN_INTERVAL_ID, foreignAutoRefresh = true)
    private Interval interval;

    public IntervalForTemplateJoin() {
    }

    public IntervalForTemplateJoin(IntervalTemplate template, Interval interval) {
        this.template = template;
        this.interval = interval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public IntervalTemplate getTemplate() {
        return template;
    }

    public void setTemplate(IntervalTemplate template) {
        this.template = template;
    }

    public Interval getInterval() {
        return interval;
    }

    public void setInterval(Interval interval) {
        this.interval = interval;
    }
}
