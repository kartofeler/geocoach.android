package pl.szmajnta.geocoach.app.Interfaces;

import pl.szmajnta.geocoach.app.Model.Entities.Interval;

/**
 * Created by Andrzej Laptop on 2014-05-23.
 */
public interface IntervalManagerInterface {
    public int getIntervalsCount();
    public Interval getCurrentInterval();
    public Interval  getNextInterval();
    public Interval getPreviousInterval();
    public Interval switchToNextInterval();
    public int getCurrentIntervalIndex();
    public float getDistanceToNextInterval();
    public int getTimeToNextInterval();
    public boolean checkSwitchPossible(float distance);
    public boolean checkSwitchPossible(int seconds);
}
