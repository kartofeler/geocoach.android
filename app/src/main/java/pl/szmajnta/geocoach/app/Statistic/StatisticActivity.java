package pl.szmajnta.geocoach.app.Statistic;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.CommonHelpers.ListHelper;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDataRepository;
import pl.szmajnta.geocoach.app.Model.DatabaseAccess.TrackerDatabaseHelper;
import pl.szmajnta.geocoach.app.Model.Entities.Training;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.Tracking.Activities.SummaryActivity;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class StatisticActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> {
    private TrackerDataRepository repo;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_statistic);

        repo = getHelper();

        List<Training> allTrainings = repo.getAllTraining();

        final ListView list = (ListView)findViewById(R.id.trainingsListView);
        list.setAdapter(new StatisticAdapter(ListHelper.iterableReverseList(allTrainings)));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(StatisticActivity.this, SummaryActivity.class);
                Training training = (Training)list.getAdapter().getItem(i);
                intent.putExtra("trainingId", training.getId());
                startActivity(intent);
            }
        });
    }

    private class StatisticAdapter extends BaseAdapter {

        private List<Training> trainings;
        private StatisticAdapter(List<Training> trainingList) {
            this.trainings = trainingList;
        }

        @Override
        public int getCount() {
            return trainings.size();
        }

        @Override
        public Object getItem(int i) {
            return trainings.get(i);
        }

        @Override
        public long getItemId(int i) {
            return trainings.get(i).getId();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            // TODO: Zrobić to sensownie, jakieś dane podstawowe wyświetlać
            LayoutInflater layoutInflater = StatisticActivity.this.getLayoutInflater();
            View convertView = layoutInflater.inflate(R.layout.item_template, viewGroup, false);

            Training template = trainings.get(i);

            TextView first = (TextView) convertView.findViewById(R.id.firstLine);
            TextView second = (TextView) convertView.findViewById(R.id.secondLine);
            TextView number = (TextView) convertView.findViewById(R.id.tvNumber);

            first.setText(template.getName());
            if(template.getStatistic() != null) {
                second.setText(String.valueOf(template.getStatistic().getTotalTime()));
                number.setText(String.valueOf(template.getStatistic().getTotalDistance()));
            }
            return convertView;
        }
    }
}