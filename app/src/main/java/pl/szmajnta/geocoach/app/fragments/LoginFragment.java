package pl.szmajnta.geocoach.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dd.processbutton.iml.ActionProcessButton;
import org.androidannotations.annotations.*;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.api.rest.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import org.springframework.web.client.HttpClientErrorException;
import pl.szmajnta.geocoach.app.R;
import pl.szmajnta.geocoach.app.beans.GeocoachUser;
import pl.szmajnta.geocoach.app.globals.EventBus;
import pl.szmajnta.geocoach.app.globals.UserPreferences_;
import pl.szmajnta.geocoach.app.globals.busevents.ChangeFragmentEvent;
import pl.szmajnta.geocoach.app.restapi.SecurityApi;
import pl.szmajnta.geocoach.app.restapi.model.request.LoginData;
import pl.szmajnta.geocoach.app.restapi.model.response.LoginResponse;

@EFragment(R.layout.fragment_login)
public class LoginFragment extends Fragment implements RestErrorHandler {

    @Bean GeocoachUser user;
    @RestService SecurityApi securityRest;
    @ViewById(R.id.buttonLogin) ActionProcessButton loginButton;
    @ViewById EditText editTextEmail;
    @ViewById EditText editTextPassword;
    @Pref UserPreferences_ userCredentials;

    @Bean EventBus bus;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    public LoginFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @AfterInject
    void registerFragmentToBus(){
        securityRest.setRestErrorHandler(this);
    }

    @Click(R.id.buttonLogin)
    void loginAction(){
        String email = editTextEmail.getText().toString();
        String pass = editTextPassword.getText().toString();
        if(email.isEmpty() || pass.isEmpty()){
            animateEditTextsWithYoyo(Techniques.Tada);
            return;
        }
        setButtonLoadMode(ActionProcessButton.Mode.ENDLESS);
        setButtonLoadProgress(50);
        setButtonText(getString(R.string.rest_api_loading));
        hideKeyboard();
        sendLoginRequest(new LoginData(email, pass));
    }

    @UiThread
    void animateEditTextsWithYoyo(Techniques techniques){
        YoYo.AnimationComposer composer = YoYo.with(techniques)
                .duration(1000);
        composer.playOn(editTextEmail);
        composer.playOn(editTextPassword);
    }

    @UiThread
    void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(loginButton.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @UiThread
    void setButtonLoadMode(ActionProcessButton.Mode mode){
        loginButton.setMode(mode);
    }

    @UiThread
    void setButtonLoadProgress(int progress){
        loginButton.setProgress(progress);
    }

    @UiThread
    void showToast(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void setButtonText(String message){
        loginButton.setText(message);
    }

    @Background
    void sendLoginRequest(LoginData credentials){
        LoginResponse response = securityRest.loginAction(credentials);
        if(response != null){
            user.setToken(response.getExtras());
            userCredentials.edit()
                    .email().put(credentials.getEmail())
                    .password().put(credentials.getPassword())
                    .apply();
            bus.post(new ChangeFragmentEvent(new MenuFragment_()));
        }
    }

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {
        setButtonLoadMode(ActionProcessButton.Mode.PROGRESS);
        setButtonLoadProgress(-1);
        if(e instanceof NestedRuntimeException) {
            HttpClientErrorException error = (HttpClientErrorException) e;
            String message;
            switch (error.getStatusCode().value()){
                case 400: message = "Niepoprawny e-mail lub hasło"; break;
                case 403: message = "Niepoprawny e-mail lub hasło"; break;
                case 500: message = "Serwis jest niedostępny"; break;
                default: message = "Wystąpił błąd. Spróbuj ponownie!";
            }
            setButtonText(message);
        }
    }
}
