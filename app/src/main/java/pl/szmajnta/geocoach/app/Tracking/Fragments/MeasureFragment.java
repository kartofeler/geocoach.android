package pl.szmajnta.geocoach.app.Tracking.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import pl.szmajnta.geocoach.app.Interfaces.MeasureScreen;
import pl.szmajnta.geocoach.app.Model.MeasureSample;
import pl.szmajnta.geocoach.app.R;

import java.text.DecimalFormat;

/**
 * Created by Andrzej Laptop on 2014-05-03.
 */
public class MeasureFragment extends BaseTrackerFragment implements MeasureScreen {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_measure, container, false);
        return view;
    }

    @Override
    public void newLocation(int time, MeasureSample sample) {

        TextView meanPaceTv = (TextView)getView().findViewById(R.id.meanPaceTV);
        TextView distanceTv = (TextView)getView().findViewById(R.id.distanceTV);

        float pace = (float)(60/(sample.meanVelocity * 3.6));

        DecimalFormat formatter = new DecimalFormat("#.00");
        distanceTv.setText(formatter.format(sample.totalDistance));
        meanPaceTv.setText(formatter.format(pace));

        newTime(time);
    }

    @Override
    public void newTime(int seconds) {
        int[] ints = splitSeconds(seconds);
        DecimalFormat formatter = new DecimalFormat("00");
        setTime(ints[0] + ":" + formatter.format(ints[1]) + ":" + formatter.format(ints[2]));
    }

    private void setTime(String time){
        TextView timeTv = (TextView)getView().findViewById(R.id.timeTV);
        timeTv.setText(time);
    }

    private int[] splitSeconds(int time) {
        int hours, minutes, seconds;

        hours = time / 3600;
        minutes = (time % 3600) / 60;
        seconds = (time % 3600) % 60;

        int[] arr = new int[3];

        arr[0] = hours;
        arr[1] = minutes;
        arr[2] = seconds;

        return arr;
    }
}