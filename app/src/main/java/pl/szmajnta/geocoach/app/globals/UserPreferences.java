package pl.szmajnta.geocoach.app.globals;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Andrzej on 2015-04-19.
 */
@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface UserPreferences {
    String email();
    String password();
}
