package pl.szmajnta.geocoach.app.Model;

import pl.szmajnta.geocoach.app.Interfaces.TrainingModelListener;
import pl.szmajnta.geocoach.app.Interfaces.TrainingProgressCallback;
import pl.szmajnta.geocoach.app.Interfaces.TrainingProgressKeeper;
import pl.szmajnta.geocoach.app.Model.Entities.TrainingNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-06-08.
 */
public abstract class BaseProgressArchiver implements TrainingModelListener, TrainingProgressKeeper {

    protected int gpsInterval;

    protected List<Double> paceHistory;
    protected List<Double> altitudeHistory;
    protected List<Double> distanceHistory;

    public BaseProgressArchiver(int gpsInterval) {
        paceHistory = new ArrayList<Double>();
        altitudeHistory = new ArrayList<Double>();
        distanceHistory = new ArrayList<Double>();

        this.gpsInterval = gpsInterval;
    }

    @Override
    public void locationChangedEvent(int time, MeasureSample measureSample) {

    }

    @Override
    public void directionChangedEvent(float zAxis, float distance) {

    }

    @Override
    public void northChangedEvent(float zAxis) {

    }

    @Override
    public void nodeChangedEvent(int currentNodeIndex, int nodesCount, TrainingNode[] nodes) {

    }

    @Override
    public void timeChangedEvent(int seconds) {

    }

    @Override
    public void gpsStatusChanged(float distance) {

    }

    @Override
    public List<Double> getPaceHistory() {
        return paceHistory;
    }

    @Override
    public List<Double> getDistanceHistory() {
        return distanceHistory;
    }

    @Override
    public List<Double> getAltitudeHistory() {
        return altitudeHistory;
    }

    @Override
    public List<Double> getKmsTimes() {
        return null;
    }

    @Override
    public void getBestTimeForOneKm(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestTimeForTwoKm(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestTimeForFiveKm(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestTimeForTenKm(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestTimeForTwentyKm(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestTimeForMaraton(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestDistanceForFiveMin(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestDistanceForCouper(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestDistanceForHalfHour(TrainingProgressCallback callback) {

    }

    @Override
    public void getBestDistanceForHour(TrainingProgressCallback callback) {

    }


}
